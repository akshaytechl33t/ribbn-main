<?php $pageTitle = 'Partner Login'; include("includes/header.php"); ?>
<nav class="navbar navbar-default navbar-me" style="background-color: white; position: static;">
    <div class="container-fluid "> 
      <div class="navbar-header">
        <button id="trigger" type="button" class="navbar-toggle collapsed menu-collapsed-button" data-toggle="collapse" data-target="#navbar-primary-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
 <a class="navbar-brand site-logo" href="index.php" id="home" ><img id="newlogo" data-target="index.php" src="images/ribbn_newlogo2x.png" width="150" height="50" alt="" style="padding-top: 0.2em; padding-bottom: 0.2em" /></a> </div>
           <div class="collapse navbar-collapse " id="navbar-primary-collapse">
              <ul class="nav navbar-nav">
                <li> <a id="menu_first_link" class="menu_links" href="index.php#whoweare" style="font-weight: 600;">WHO WE ARE</a> </li>
                <li class="margin-Top10">.</li>
                <li> <a class="menu_links" href="index.php#whatwedo" style="font-weight: 600;">WHAT WE DO</a> </li>
                <li class="margin-Top10">.</li>
                <li> <a class="menu_links" href="index.php#patients" style="font-weight: 600;">WHAT WE OFFER</a> </li>
                <li class="margin-Top10">.</li>
                <li> <a class="menu_links" href="index.php#referapatients" style="font-weight: 600;">REFER A PATIENT</a> </li>
                <li class="margin-Top10">.</li>
                <li> <a class="menu_links" href="index.php#partners" style="font-weight: 600;">PARTNER WITH US</a> </li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li>
                <a href="index.php#haveQuery" class="menu_links" data-target="index.php#haveQuery" style="font-weight: 600; padding-top: 18px;">Contact Us</a>
                </li>
                <li class="margin-Top-sr"></li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="partner_login_cont">

      <div class="row text-center partner_login_row">
        <div class="partner_top">
          <span style="align-self: flex-end; padding-right: 15px; font-size: 30px; color: #aaa">&times;</span>
          <h4 style="align-self: center; margin-top: 30px; font-size: 22px"><b>Partner Login</b></h4>
        </div>
        <form class="text-center partner_login_form" autocomplete="off">
          <div class="form-group">
            <div class="form-group-email">
                <img src="" style="height: 30px; width: 30px">
                <input id="input_id" type="email" class="newform form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your Registered Mobile Number / Email" autocomplete="off">  
            </div>
            <small id="emailHelp" class="form-text text-muted"></small>
          </div>
          <div class="form-group">
            <div class="form-group-email">
                <img src="" style="height: 30px; width: 30px">
                <input type="password" class="newform form-control" id="exampleInputPassword1" placeholder="Password" autocomplete="off">
            </div>
            <small id="passwordHelp" class="form-text text-muted">This field is required.</small>
          </div>
          <div class="form-check" style="align-self: flex-end; padding-right: 15px">
            <a href="#" style="text-decoration: none; font-size: 14px; color: #2f3790">Forgot Password?</a>
          </div>
          <button type="submit" class="btn submit_btn">Submit</button>
        </form>
      </div>

      <div class="row text-center" style="height: 20%; width: 100%; margin-left:0; background-color: #2f3790; color: white">
        <p style="font-size: 12px; width: 75%; margin: 0 auto"><a style="color: white" href="">Not Registered Yet?</a></p>
        <p style="font-size: 12px; width: 75%; margin: 0 auto"><a style="color: white" href="">Are You an Ambulance Provider or a Nurse Care Provider or a Hospital?</a></p>
        <p style="font-size: 14px; width: 75%; margin: 0 auto"><a style="color: white" href="">Please Register Here</a></p>
      </div>

    </div>


    <footer class="privacy_footer">
              <div class="row footer_class">
               <div class="col-md-4 col-sm-12 col-xs-12 text-center copyright_class">
                  <p style="color: black; text-align: justify; letter-spacing: 0.5px"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.<br></p><span style="color: black;  font-size: 14px; letter-spacing: 0.5px">All Rights Reserved.</span>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="social_icons">
                     <a target="_blank" rel="noopener" href="https://www.facebook.com/Ribbnhealth/" class="icon_class"><i class="fa fa-facebook"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://www.linkedin.com/company/ribbn/" class="icon_class"><i class="fa fa-twitter"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://twitter.com/RibbnHealth" class="icon_class"><i class="fa fa-linkedin"></i></a>
                  </div>
               </div>
               <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv" style="order: -1">
                  <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">PRIVACY POLICY</a>
                  <a href="termsofservice.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:-1em" class="terms">TERMS OF SERVICE</a>
               </div>
               <span style="order:5; font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>
            </div>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script type="text/javascript">
      $(document).ready(function() {
        $('#trigger').click(function(event) {
            event.stopPropagation();
            $('#navbar-primary-collapse').toggle();

            $(document).click(function() {
                $('#navbar-primary-collapse').hide();
            });

        });
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $(".submit_btn").on('click', function(e) {
    e.preventDefault();
    if ($("#input_id").val() == "") {
      $("#emailHelp").text("hi");
      $("#passwordHelp").text("hi");
    }
  })
});
</script>
</body>
</html>