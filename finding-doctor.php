<?php $pageTitle = 'Home Care'; include("includes/header.php"); ?>
<nav class="navbar navbar-default navbar-me">
    <div class="container-fluid ">
       <div class="navbar-header">
          <button id="trigger" type="button" class="navbar-toggle collapsed menu-collapsed-button press-to-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand site-logo oldlogo" href="index.php" id="home"><img src="images/logo.png" data-target="#fh5co-page" class="" width="103" height="165" alt=""/></a> <a class="navbar-brand site-logo new-logo" href="index.php" id="home" ><img id="newlogo" data-target="#fh5co-page" src="images/ribbn_newlogo2x.png" width="150" height="50" alt="" style="padding-top: 0.2em; padding-bottom: 0.2em" /></a> 
       </div>
       <div class="collapse navbar-collapse " id="navbar-primary-collapse">
          <ul class="nav navbar-nav">
             <li> <a class="" href="index.php#whoweare" style="font-weight: 600;">WHO WE ARE</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#whatwedo" style="font-weight: 600;">WHAT WE DO</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#patients" style="font-weight: 600;">WHAT WE OFFER</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#referapatients" style="font-weight: 600;">REFER A PATIENT</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#partners" style="font-weight: 600;">PARTNER WITH US</a> </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li>
                 <a href="index.php#footer" style="font-weight: 600; padding-top: 18px; padding-bottom: 1.5rem" class="contactbtn">Contact Us</a>
              </li>
              <li class="margin-Top-sr"></li>
           </ul>
       </div>
    </div>
 </nav>
<div class="container-fluid med-page-banner nurse" style="overflow-x: hidden;">
    <div class="med-banner-icon nurse">
        <img src="images/blog-icon.svg">
    </div>
    <h2> Ribbn Blog </h2>
</div>
<div class="container-fluid clearfix mob-container shrunk-container" style="background-color: white; color: black;text-align: left; padding-top: 3%;overflow-x: hidden;" id="tos_cont">
    <!-- home care -->
    <div class="col-md-9 col-sm-12 no-padding-left homecareshow homecare no-xs-padding-r">
        <div class="col-sm-12 no-padding-left">
          <h2 class="med-page-head text-bold text-left" style="font-size: 24px;">  
              Finding the RIGHT doctor - by VN Bhattacharya
          </h2>
        </div>
        
        <div class="para-content clear">
            <p class="no-bottom-gap"> 
                Most people, especially managers, associate decision-making with work. This is a narrow view. Many of our most important decisions are about our health, wealth, and happiness. Consider the need to find the RIGHT doctor if you or a family member has a life threatening or debilitating condition that requires major surgery. In such situations, we often choose a large and reputable hospital and trust the specialist they assign. Sometimes we ask friends and associates to recommend a doctor who has treated someone they know for a similar condition.
            </p>
            <h2 class="blog-sub-head text-left">  
              How biases take root
            </h2>
            <p> 
              Choice of hospital is usually coloured by proximity and convenience of family members who will need to attend to the hospitalised patient (a peculiar Indian circumstance). The hospital is often and incorrectly considered as the proxy for the doctor’s expertise. A large hospital usually has many doctors in major specialities. The one we end up meeting may be randomly assigned. Tiny sample size bedevils recommendations of friends and associates. Experiences of a couple of patients are hardly a reliable indicator of a doctor’s expertise. Hospital websites showcase glowing profiles of their specialists. How much should we trust them? How equipped are we to compare profiles and evaluate?
            </p>
            <h2 class="blog-sub-head text-left">  
              Where should we begin? 
            </h2>
            <p> 
              The Internet can be a starting point. Although doctors don’t like it, the Internet is a good place to learn about serious ailments, causes, and recent advances in treatment. This is never enough to evaluate a doctor’s abilities but it can prepare us for a meaningful discussion with doctors on diagnosis, treatment, and prognosis. It is usually a good idea to consult more than one specialist if one has the time. It helps gather a broader range of opinion. If their assessments converge, one can be confident the proposed line of treatment is the correct one. If they diverge, it will point to the need for a deeper dive. Yes it will make it harder to choose the doctor and treatment, but it will probably be better than a single opinion.
            </p>
            <h2 class="blog-sub-head text-left">  
              When sizzle is not substance
            </h2>
            <p> 
              Perhaps the most acute problem with finding the right surgeon or specialist is the effect of bias on our judgement. We may be impressed by a hospital’s snazzy facilities, courteous, friendly staff and assume doctors are more competent than elsewhere. The ambience of the doctor’s office, his demeanour, substance and form of his advice shape our judgement.
            </p>
            <p> 
              We like doctors who listen carefully, and appear reassuring. We tend to consider them more capable even though their behaviour is not a reliable indicator of professional accomplishments. The many certificates displayed on the wall may have something to say but we may not have the capacity to evaluate their relevance to our needs.
            </p>
            <p> 
              A friend of mine was dismayed to find a surgeon’s prognosis was overly positive when compared with three others he had consulted. He heeded that alarm bell. Our medical condition has us in a delicate frame of mind. We worry if we may antagonise the doctor with too many questions. Will he agree to treat me? Will I get the best treatment at his hands? These concerns hinder objective assessment of the expertise or suitability of the doctor. The insidious problem with cognitive bias is that we remain unaware of flawed judgement.
            </p>
            <h2 class="blog-sub-head text-left">  
              Data, and more data 
            </h2>
            <p>
              First, get more data. Read, research, ask knowledgeable people – other doctors for example – and learn more about the ailment, treatment, etc.
            </p>
            <p>
              Find and list prospective specialists. Get to know more about them, consult and discuss with them approaches to treatment. Gain insight on what may work well, and what may not. Do not hesitate to ask questions. Many doctors appreciate having to deal with knowledgeable patients. Finally, find a wise person who is not likely to be biased by your impressions, to be your sounding board. He will challenge your thinking, assumptions, and uncover errors of judgement, illusions, and delusions. Decisions pertaining to our health are some of the most important we make. We simply cannot afford mistakes. Admittedly, it is difficult. That is why it merits greater diligence and objectivity.
            </p>
            <p>
              The Author, VN Bhattacharya, is an independent consultant on business and corporate strategy. He can be reached on <a href="mailto:vn@vnbhattacharya.com">vn@vnbhattacharya.com </a>
            </p>
            <p class="med-sub-title font-size-17 text-bold clearfix text-xs-center">
              Click 
              <a href="http://www.vnbhattacharya.com/?p=13121" target="_blank"> here </a> 
              to read the main article.
               <a class="back-list" href="blog-headings.php"> Back to List </a> 
            </p>
        </div>
        <!-- <div class="second-list clearfix nurse">
            <h3 class="med-sub-title font-size-17 text-bold"> 
                To know more E-mail <a href="mailto:contact@ribbn.in" target="_blank">contact@ribbn.in</a> or call +91 9964321682.
            </h3>
        </div> -->
    </div>
    <div class="col-md-3 col-sm-12 homecareshow homecare">

        <!-- <div class="col-sm-3 col-sm-offset-1 no-padding-right no-padding-left"> -->
          <button class="thum-btn" type="button" data-toggle="modal" data-target="#requestANurse"> 
            <img src="images/Pointing-Finger.svg" class="pointingFingerClass"> <span class="pointingFingerSpanClass margin-left-min-10" > Request a Home Nurse </span>
            <!-- style="margin-right: auto; margin-left: -30px;" -->
          </button>
        <!-- </div> -->
        
        <a href="home-care.php">
          <div class="each-article-thumb">
              <div class="second-overlay"> </div>
              <img src="images/smallthumb-two.jpg">
              <div class="art-thumb-overlay">
                  <span class="top-title"> 
                  DID YOU KNOW
                  </span>
                  <p> 
                      Chances of nosocomial infections is lesser at home.
                  </p>
              </div>
          </div>
        </a>
        <a href="medical-equipments.php">
          <div class="each-article-thumb read-also">
              <div class="second-overlay"> </div>
              <img src="images/smallthumb-one.jpg">
              <div class="art-thumb-overlay nurse-read-more">
                  <span class="top-title"> 
                  ALSO READ
                  </span>
                  <p> 
                     Continued hospital stay post-surgery/ stabilization proves expensive for the hospital as well as the patient.
                  </p>
              </div>
          </div>
        </a>
    </div>
    <p class="med-article-title">
      <span>  
      OTHER SERVICES
      </span>
  </p>
  <div class="bottom-icon-wrapper">
    <a href="concierge.php">
      <div class="each-buttom-icon">
          <img class="reverse-image" src="images/AMBULANCE.svg">
          <p> AMBULANCE </p>
      </div>
    </a>
    <a href="medical-equipments.php">
      <div class="each-buttom-icon">
          <img src="images/medicines.svg">
          <p> MED EQUIPMENT </p>
      </div>
    </a>
    
  </div> 
</div>


<?php  include("includes/modals.php"); ?>
<footer class="privacy_footer">
<!--   <div class="container-fluid">
    <div class="row" style="height: 60px">
       <div class="col-md-4 col-sm-12 col-xs-12 text-center" style="display: flex; justify-content: center;">
          <p style="color: black; text-align: justify;"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.<br><span class="rights">All Rights Reserved.</span></p>
       </div>
       <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="social_icons">
             <a href="https://www.facebook.com/Ribbnhealth/"><img src="images/facebook-logo.svg" style="width: 30px; height: 30px; margin-left: 10px"></a>
             <a href="https://www.linkedin.com/company/ribbn/"><img src="images/linkedin.svg" style="width: 30px; height: 30px; margin-left: 20px"></a>
             <a href="https://twitter.com/RibbnHealth"><img src="images/twitter-social.svg" style="width: 30px; height: 30px; margin-left: 20px"></a>
          </div>
       </div>
       <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv">
          <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">Privacy Policy</a>
          <a href="termsofservice.php" style="color: #2f3790; font-size: 14px; margin-right:5px" class="terms">Terms of Service</a>
          <span style="font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>
       </div>
    </div>
  </div> -->


              <div class="row footer_class">
               <div class="col-md-4 col-sm-12 col-xs-12 text-center copyright_class">
                  <p class="copyright_p for-desk-top"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>

                  <p class="copyright_p for-mobile"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="social_icons">
                     <a target="_blank" rel="noopener" href="https://www.facebook.com/Ribbnhealth/" class="icon_class"><i class="fa fa-facebook"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://www.linkedin.com/company/ribbn/" class="icon_class"><i class="fa fa-linkedin"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://twitter.com/RibbnHealth" class="icon_class"><i class="fa fa-twitter"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="blog-headings.php" class="icon_class">
                        <i class="fa fa-rss-square" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv" style="order: -1">
                  <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">PRIVACY POLICY</a>
                  <a href="termsofservice.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:-1em" class="terms">TERMS OF SERVICE</a>
               </div>
               <!-- <span style="order:5; font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>-->
            </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
 -->

<script type="text/javascript" src="css/bootstrap.min.js"></script>

<script type="text/javascript">
        $(document).ready(function() {
        $('#trigger').click(function(event) {
            event.stopPropagation();
            $('#navbar-primary-collapse').toggle();

            $(document).click(function() {
                $('#navbar-primary-collapse').hide();
            });

        });
    });
</script>

<script type="text/javascript">
  $(document).ready(function() {
      function parse_query_string(query) {
        var vars = query.split("&");
        var query_string = {};
        for (var i = 0; i < vars.length; i++) {
          var pair = vars[i].split("=");
          // If first entry with this name
          if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
          } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
          } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
          }
        }
        return query_string;
      };

      // $(".homecareshow.homecare").show();
      // $(".homecareshow.palliativecare").hide();
  });

  $(window).scroll(function() {
        if ($(window).width() > 500) {
            if ($(this).scrollTop() > 5) {
                $(".navbar-me").addClass("fixed-me");
                $('nav').addClass('shrink');
                $('.oldlogo').hide();
                $('.new-logo').show();
            } else {
                $(".navbar-me").removeClass("fixed-me");
                $('nav').removeClass('shrink');
                $('.oldlogo').show();
                $('.new-logo').hide();
            }
        }
    });
</script>

<script>

       $('.modal-checkbox').click(function() {
        if ($(this).is(':checked')) {
           $(this).parents('form').find('.modal-submit-btn').removeAttr('disabled');
        } else {
           $(this).parents('form').find('.modal-submit-btn').attr('disabled', 'disabled');
        }
    });

       $('#trigger').click(function(){

          $(this).parent().next('.navbar-collapse').toggleClass('display-blck');

      });
</script>
<?php include("includes/footer.php"); ?>
</body>
</html>
