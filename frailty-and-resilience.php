<?php $pageTitle = 'Medicine and Medical Equipments'; include("includes/header.php"); ?>
<nav class="navbar navbar-default navbar-me">
    <div class="container-fluid ">
       <div class="navbar-header">
          <button id="trigger" type="button" class="navbar-toggle collapsed menu-collapsed-button press-to-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse" aria-expanded="false"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
          </button>
          <a class="navbar-brand site-logo oldlogo" href="index.php" id="home"><img src="images/logo.png" data-target="#fh5co-page" class="" width="103" height="165" alt=""/></a> <a class="navbar-brand site-logo new-logo" href="index.php" id="home" ><img id="newlogo" data-target="#fh5co-page" src="images/ribbn_newlogo2x.png" width="150" height="50" alt="" style="padding-top: 0.2em; padding-bottom: 0.2em" /></a> 
       </div>
       <div class="collapse navbar-collapse " id="navbar-primary-collapse">
          <ul class="nav navbar-nav">
             <li> <a class="" href="index.php#whoweare" style="font-weight: 600;">WHO WE ARE</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#whatwedo" style="font-weight: 600;">WHAT WE DO</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#patients" style="font-weight: 600;">WHAT WE OFFER</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#referapatients" style="font-weight: 600;">REFER A PATIENT</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#partners" style="font-weight: 600;">PARTNER WITH US</a> </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li>
                 <a href="index.php#footer" style="font-weight: 600; padding-top: 18px; padding-bottom: 1.5rem" class="contactbtn">Contact Us</a>
              </li>
              <li class="margin-Top-sr"></li>
           </ul>
       </div>
    </div>
 </nav>
<div class="container-fluid med-page-banner" style="overflow-x: hidden;">
    <div class="med-banner-icon">
        <img src="images/blog-icon.svg">
    </div>
    <h2> Ribbn Blog </h2>
</div>
<div class="container-fluid clearfix mob-container shrunk-container" style="background-color: white; color: black;text-align: left; padding-top: 3%;overflow-x: hidden;" id="tos_cont">
    <div class="col-md-9 col-sm-12 no-padding-left no-xs-padding-r">
      <div class="col-sm-12 no-padding-left">
        <h2 class="med-page-head text-bold text-left font-size-24">  
            On frailty and resilience - by Srikanth Rajagopalan
        </h2>
      </div>

      <div class="para-content clear">
          <p class="no-bottom-gap"> 
               Most of us have a rather simplistic and well-intentioned view of our health: get enough sleep, eat sensibly, manage stress, get enough exercise, spend time with people you love … focus on the essentials, and you’ll be fine. Except that what you see is not quite what you get; once in a while, a hidden bomb turns up that’s not as scary as the C word, but just as lethal. This is my story of discovering a silent killer, connecting the dots looking backwards in Steve Jobsian fashion, and the eye-opening lessons for the rest of my life. If any one of you, having read this post, instead of sympathising with me takes your health less for granted, I’ve done a little bit to give back for this universe’s abundance towards me.
          </p>
          
          <p>
            Sunday 27th August ~ 4 a.m. I’m convulsed with seizures, frothing at the mouth and I’ve soiled my shorts. Wife, neighbours and prayers fail to revive me, so I’m stretchered off to hospital. Thanks to a wife smarter than I, an 82 year old Dad living with us, and a hospital in the same neighbourhood, we are always prepared, and I’m in the emergency ward within fifteen minutes, and conscious in 3 hours or so.
          </p>

          <p>
            Turns out that I’ve been suffering from obstructive sleep apnea for the past few years. I have a deformity that causes the back of the roof of my mouth and my tongue to come together when I’m sleeping; as a result, my windpipe shuts off. The brain reacts by shaking me awake, my heart reacts by working 10X as hard as it should be. I wake up 32 times an hour, have a blood oxygen level of 89% (reference value = 98%), and 7% of R.E.M. (deep) sleep (reference value = 28%). In other words, I get up every two minutes; while I might be in bed for 8 hours, I get about 40 minutes of sleep. And I risk choking to death. Every night for the past four years or so.
          </p>

          <h2 class="med-page-head text-left font-size-20"> Without a clue. </h2>

          <p>
            Instant flashbacks occur of close friends who died suddenly in their sleep (95% of these cases are because of sleep apnea). Of being inexplicably tired through the day, and having trouble concentrating in meetings. Of giving up willfully in the third set of a TT game with Rajeev. Of the stubborn five kilos and two inches that have clung to me like leeches for the past three years. The less-than-effective meditation, yoga, and gym routines. Like driving with your handbrake firmly locked, but changing tyres, tuning engine, and using 98-octane petrol to try and go faster.
          </p>

          <h2 class="med-page-head text-left font-size-20"> The scary part is not having a clue. Until now. </h2>

          <p> 
            The good news is that I’ve root-caused my problem, and found a competent sleep expert. The solution is relatively simple: wear a mandibular implant that looks like a boxer’s tooth guard when I sleep. The idea is to keep the jaws open so that I keep breathing. The best part? I’ve decided to get better, and once I fix the basics, I can bring my health back under my control.
          </p>

          <em class="dark-grey-text bottom-gap display-inline-block"> It only gets better from here. </em>

          <div class="second-list clearfix">

          <h3 class="med-page-head text-left font-size-20"> 
            A few things can get better. As you read this, do us a favour: 
          </h3>

          <ul>
              <li> Don’t sympathise with me, but introspect for yourself. See if you identify with any of the symptoms that I’ve been putting up with. </li>
              <li> Be honest with yourself: are you making it worse by being overweight, sedentary, or smoking? </li>
              <li> Ask your spouse or partner if you snore or gasp for breath at night. If you answered Yes to either, ACT NOW, start with the basics. </li>
              <li> Spread the word. Sleep apnea affects 10% of India’s adult population. Awareness is in the decimal %. Undetected and/or untreated, sleep apnea is statistically more lethal than, and can cause, strokes or heart failure. It is more likely to catch up with you in your forties. </li>
          </ul>

          </div>
          
          
          <p class="text-left med-sub-title font-size-17 text-bold clearfix text-xs-center">
            Click <a href="https://medium.com/@srikanthrajag/on-frailty-and-resilience-484ded3c5f12" target="_blank"> here </a> to read the main article.
            <a class="back-list" href="blog-headings.php"> Back to List </a>
          </p>

          <!-- <div class="second-list clearfix nurse">
              <h3 class="med-sub-title font-size-17 text-bold"> 
                  To know more E-mail <a href="mailto:contact@ribbn.in" target="_blank">contact@ribbn.in</a> or call +91 9964321682.
              </h3>
          </div> -->

      </div>
<!--       <div class="medical-equip-table">
          <table>
              <tr>
                  <th> Standard Equipment </th>
                  <th> Speciality Equipment </th>
              </tr>
              <tr>
                  <td>
                      <ul>
                          <li> Bedpans, Bandages/ Gauze strips </li>
                          <li> Canes/ Crutches/ Walkers/ Wheelchairs (Manual/ Automated </li>
                          <li> Cots – Manual and Automated </li>
                          <li> Mattresses </li>
                          <li> Nebulizers and other respiratory devices </li>
                          <li> IV Apparatus – Stand, Canula </li>
                      </ul>
                  </td>
                  <td>
                      <ul>
                          <li> Medical Devices </li>
                          <li> Costom Manual + Power Wheelchairs </li>
                          <li> Bath and Shower Equipment </li>
                          <li> Patient Warmer </li>
                          <li> Oxygen Concentrator, Oxygen Cylinder with Monitors </li>
                      </ul>
                  </td>
              </tr>
          </table>
      </div> -->
<!--       <div class="second-list clearfix">
        
          <h3 class="med-sub-title text-bold" style="margin-top: 20px;"> 
              Did you know that there are several kinds of mattresses 
          </h3>
        
          <ul>
              <li> Memory Foam. </li>
              <li> Gel. </li>
              <li> Pillow Tops. </li>
              <li> Innerspring. </li>
              <li> Water Bed. </li>
              <li> Adjustable Bases. </li>
          </ul>
      </div> -->

      <!-- <p class="final-med-cont"> 
          <span class="sub">Please Contact:</span> <a href="mailto:medequipment@ribbn.in"> medequipment@ribbn.in </a>  and let us help you choose the right equipment.
      </p> -->

  </div>
  <div class="col-md-3 col-sm-12">

      <!-- <div class="col-sm-3 no-padding-right no-padding-left"> -->
          <button class="thum-btn" type="button" data-toggle="modal" data-target="#requestANurse"> 
            <img src="images/Pointing-Finger.svg" class="pointingFingerClass"> <span class="pointingFingerSpanClass margin-left-min-10" > Request a Home Nurse </span>
            <!-- style="margin-right: auto; margin-left: -30px;" -->
          </button>
      <!-- </div> -->

      <a href="home-care-palliative.php">
        <div class="each-article-thumb">
            <div class="second-overlay"> </div>
            <img src="images/smallthumb-three.jpg">
            <div class="art-thumb-overlay">
                <span class="top-title"> 
                DID YOU KNOW
                </span>
                <p> 
                  Hospice care is only for patients who are no longer receiving curative treatments for their illness, and want to focus only on quality of life.
                </p>
            </div>
        </div>
      </a>
      <a href="concierge.php">
        <div class="each-article-thumb read-also">
            <div class="second-overlay"> </div>
            <img src="images/hospital_discharge.jpg">
            <!-- <img src="images/hospital_discharge.jpg" class="img-responsive" style="height: 197px;" width="100%" alt=""/> -->
            <div class="art-thumb-overlay nurse-read-more">
                <span class="top-title"> 
                ALSO READ
                </span>
                <p> 
                    Finding a vehicle to take you home on the day of your discharge can be a harrowing experience!
                </p>
            </div>
        </div>
      </a>
  </div>

   <p class="med-article-title"> 
      <span>  
      OTHER SERVICES
      </span>
  </p>
  <div class="bottom-icon-wrapper">
    <a href="concierge.php">
      <div class="each-buttom-icon">
          <img class="reverse-image" src="images/AMBULANCE.svg">
          <p> AMBULANCE </p>
      </div>
    </a>
    <a href="medical-equipments.php">
      <div class="each-buttom-icon">
          <img src="images/medicines.svg">
          <p> MED EQUIPMENT </p>
      </div>
    </a>
  </div>
</div>
<?php  include("includes/modals.php"); ?>
<footer class="privacy_footer">
<!--   <div class="container-fluid">
    <div class="row" style="height: 60px">
       <div class="col-md-4 col-sm-12 col-xs-12 text-center" style="display: flex; justify-content: center;">
          <p style="color: black; text-align: justify;"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.<br><span class="rights">All Rights Reserved.</span></p>
       </div>
       <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="social_icons">
             <a href="https://www.facebook.com/Ribbnhealth/"><img src="images/facebook-logo.svg" style="width: 30px; height: 30px; margin-left: 10px"></a>
             <a href="https://www.linkedin.com/company/ribbn/"><img src="images/linkedin.svg" style="width: 30px; height: 30px; margin-left: 20px"></a>
             <a href="https://twitter.com/RibbnHealth"><img src="images/twitter-social.svg" style="width: 30px; height: 30px; margin-left: 20px"></a>
          </div>
       </div>
       <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv">
          <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">Privacy Policy</a>
          <a href="termsofservice.php" style="color: #2f3790; font-size: 14px; margin-right:5px" class="terms">Terms of Service</a>
          <span style="font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>
       </div>
    </div>
  </div> -->

              <div class="row footer_class">
               <div class="col-md-4 col-sm-12 col-xs-12 text-center copyright_class">
                  <p class="copyright_p for-desk-top"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>

                  <p class="copyright_p for-mobile"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="social_icons">
                     <a target="_blank" rel="noopener" href="https://www.facebook.com/Ribbnhealth/" class="icon_class"><i class="fa fa-facebook"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://www.linkedin.com/company/ribbn/" class="icon_class"><i class="fa fa-linkedin"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://twitter.com/RibbnHealth" class="icon_class"><i class="fa fa-twitter"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="blog-headings.php" class="icon_class">
                        <i class="fa fa-rss-square" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv" style="order: -1">
                  <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">PRIVACY POLICY</a>
                  <a href="termsofservice.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:-1em" class="terms">TERMS OF SERVICE</a>
               </div>
               <!-- <span style="order:5; font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>-->
            </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
<script src="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/js/bootstrap.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
        $('#trigger').click(function(event) {
            event.stopPropagation();
            $('#navbar-primary-collapse').toggle();

            $(document).click(function() {
                $('#navbar-primary-collapse').hide();
            });

        });
    });
</script>

<script type="text/javascript">
  $(document).ready(function() {
      function parse_query_string(query) {
        var vars = query.split("&");
        var query_string = {};
        for (var i = 0; i < vars.length; i++) {
          var pair = vars[i].split("=");
          // If first entry with this name
          if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
          } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
          } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
          }
        }
        return query_string;
      };
  });

  $(window).scroll(function() {
        if ($(window).width() > 500) {
            if ($(this).scrollTop() > 5) {
                $(".navbar-me").addClass("fixed-me");
                $('nav').addClass('shrink');
                $('.oldlogo').hide();
                $('.new-logo').show();
            } else {
                $(".navbar-me").removeClass("fixed-me");
                $('nav').removeClass('shrink');
                $('.oldlogo').show();
                $('.new-logo').hide();
            }
        }
    });
</script>

<script>

       $('.modal-checkbox').click(function() {
        if ($(this).is(':checked')) {
           $(this).parents('form').find('.modal-submit-btn').removeAttr('disabled');
        } else {
           $(this).parents('form').find('.modal-submit-btn').attr('disabled', 'disabled');
        }
    });

     $('#trigger').click(function(){

          $(this).parent().next('.navbar-collapse').toggleClass('display-blck');

      });
</script>
<?php include("includes/footer.php"); ?>


</body>
</html>
