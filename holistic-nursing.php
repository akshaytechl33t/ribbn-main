<?php $pageTitle = 'Medicine and Medical Equipments'; include("includes/header.php"); ?>
<nav class="navbar navbar-default navbar-me">
    <div class="container-fluid ">
       <div class="navbar-header">
          <button id="trigger" type="button" class="navbar-toggle collapsed menu-collapsed-button press-to-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse" aria-expanded="false"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
          </button>
          <a class="navbar-brand site-logo oldlogo" href="index.php" id="home"><img src="images/logo.png" data-target="#fh5co-page" class="" width="103" height="165" alt=""/></a> <a class="navbar-brand site-logo new-logo" href="index.php" id="home" ><img id="newlogo" data-target="#fh5co-page" src="images/ribbn_newlogo2x.png" width="150" height="50" alt="" style="padding-top: 0.2em; padding-bottom: 0.2em" /></a> 
       </div>
       <div class="collapse navbar-collapse " id="navbar-primary-collapse">
          <ul class="nav navbar-nav">
             <li> <a class="" href="index.php#whoweare" style="font-weight: 600;">WHO WE ARE</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#whatwedo" style="font-weight: 600;">WHAT WE DO</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#patients" style="font-weight: 600;">WHAT WE OFFER</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#referapatients" style="font-weight: 600;">REFER A PATIENT</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#partners" style="font-weight: 600;">PARTNER WITH US</a> </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li>
                 <a href="index.php#footer" style="font-weight: 600; padding-top: 18px; padding-bottom: 1.5rem" class="contactbtn">Contact Us</a>
              </li>
              <li class="margin-Top-sr"></li>
           </ul>
       </div>
    </div>
 </nav>
<div class="container-fluid med-page-banner" style="overflow-x: hidden;">
    <div class="med-banner-icon">
        <img src="images/blog-icon.svg">
    </div>
    <h2> Ribbn Blog </h2>
</div>
<div class="container-fluid clearfix mob-container shrunk-container" style="background-color: white; color: black;text-align: left; padding-top: 3%;overflow-x: hidden;" id="tos_cont">
    <div class="col-md-9 col-sm-12 no-padding-left no-xs-padding-r">
      <div class="col-sm-12 no-padding-left">
        <h2 class="med-page-head text-bold text-left" style="font-size: 24px;">  
            Holistic Nursing - by Shelly Ferman Autor
        </h2>
      </div>

      <div class="para-content clear">
          <p class="no-bottom-gap"> 
               Over the course of her lifelong career as a nurse, <b> Shelly Ferman Autor </b> has seen shifts in open-mindedness toward hospice and palliative care, and the benefits of both to patients in need.
          </p>
          
          <p> 
              “Our job as nurses is to meet people where they’re at and help them progress as people—it isn’t just medicine,” she says. “As nurses, we are less driven by how to cure people and place emphasis on helping patients and their families cope with whatever they’re going through.”
          </p>

          <p> 
             Autor says in the early days of her career, much of the focus as a nurse was to treat illness, injury, and disease without considering other avenues. Options such as hospice and palliative care—relieving patients from the symptoms and stress of a serious illness, rather than focusing on a cure—were not as widely recognized. Often, nurses and doctors defaulted to immediate treatment rather than improving the quality of life for both the patient and the family.
          </p>

          <p> 
              “After I graduated UMass, I got a job in the neurology unit at Boston Medical Center and saw a lot of trauma and suffering. There was rarely a discussion of what the patient wanted in terms of their care,” she recalls. “We didn’t ask them what kind of treatment they wanted, and I think they would have welcomed some of those discussions.”
          </p>

          <p> 
              It wasn’t until further along in Autor’s career after working as a geriatric nurse practitioner that she decided to shift over to the growing field of hospice and palliative care. Moved by the holistic approach to these practices, Autor felt compelled to focus on bettering the mental, emotional, and physical wellbeing for those she treated.
          </p>

          <p>
            <b>
              “Earlier in my career, you mostly saw patients receiving aggressive treatment for diseases like cancer, but now the care is so much more integrated,” she says. “As a homecare hospice nurse, I’ve seen massage therapy, dog therapy, and an openness to incorporate spirituality into someone’s plan. There’s not one-size-fits-all formula to treatment.”
            </b>
          </p>

          <p> 
              Having the opportunity to experience such an intimate time in a patient’s life has left Autor humbled and grateful for the work she does. With comfort as one the most important factors for her patients, she stresses the need to keep an open mind and heart for those who enter her life.
          </p>

          <p> 
              “I find satisfaction in walking into a chaotic house and just being okay with whatever is in there. I’m not there to judge anybody,” she explains. “I take a deep breath and take in the scene—who’s who, what’s happening, the family dynamics— and just absorb it all. It’s good to know that you are making such a difference in these people’s lives.”
          </p>

          <p> 
              For UMass nursing students interested in pursing hospice and palliative care, Autor asserts that staying positive and treating each patient’s experience with equal attention will foster a rewarding career.
          </p>

          <p> 
              “As a nurse, you can’t be afraid of death or dying. Being there and helping families at that moment is just so incredibly intimate and special,” she remarks. “It’s our job to educate people that there is not only one way to do this. Everyone has a choice about goals of care, such as hospitalization and aggressive treatment. We need to have those discussions about care options so our patients can be empowered to make their own choices.”
          </p>

          <p> 
              Reflecting on her long and fulfilling nursing career, Autor attributes the catalyst for her success to the UMass Amherst College of Nursing and takes great pride in the strides the program has made since her time on campus.
          </p>

          <p> 
              “I’m so proud of my UMass degree and the direction the Nursing program has gone. It’s hard to believe that the School of Nursing is 65 years old!” she says. “UMass helped me learn to be myself and bring my whole self to my patients, which is so important with the work we do.”
          </p>
          
          <p>
              The Author, Shelly Ferman Autor, is an alumnus of UMass Amherst College of Nursing.
            </p>
            <p class="text-left med-sub-title font-size-17 text-bold clearfix text-xs-center">
              Click <a href="https://www.umassalumni.com/s/1640/rd17/leftCol.aspx?sid=1640&gid=2&pgid=5181&cid=8621&ecid=8621&crid=0&calpgid=1978&calcid=3482" target="_blank"> here </a> to read the main article.
              <a class="back-list" href="blog-headings.php"> Back to List </a>
            </p>

          <!-- <div class="second-list clearfix nurse">
              <h3 class="med-sub-title font-size-17 text-bold"> 
                  To know more E-mail <a href="mailto:contact@ribbn.in" target="_blank">contact@ribbn.in</a> or call +91 9964321682.
              </h3>
          </div> -->

      </div>
<!--       <div class="medical-equip-table">
          <table>
              <tr>
                  <th> Standard Equipment </th>
                  <th> Speciality Equipment </th>
              </tr>
              <tr>
                  <td>
                      <ul>
                          <li> Bedpans, Bandages/ Gauze strips </li>
                          <li> Canes/ Crutches/ Walkers/ Wheelchairs (Manual/ Automated </li>
                          <li> Cots – Manual and Automated </li>
                          <li> Mattresses </li>
                          <li> Nebulizers and other respiratory devices </li>
                          <li> IV Apparatus – Stand, Canula </li>
                      </ul>
                  </td>
                  <td>
                      <ul>
                          <li> Medical Devices </li>
                          <li> Costom Manual + Power Wheelchairs </li>
                          <li> Bath and Shower Equipment </li>
                          <li> Patient Warmer </li>
                          <li> Oxygen Concentrator, Oxygen Cylinder with Monitors </li>
                      </ul>
                  </td>
              </tr>
          </table>
      </div> -->
<!--       <div class="second-list clearfix">
        
          <h3 class="med-sub-title text-bold" style="margin-top: 20px;"> 
              Did you know that there are several kinds of mattresses 
          </h3>
        
          <ul>
              <li> Memory Foam. </li>
              <li> Gel. </li>
              <li> Pillow Tops. </li>
              <li> Innerspring. </li>
              <li> Water Bed. </li>
              <li> Adjustable Bases. </li>
          </ul>
      </div> -->

      <!-- <p class="final-med-cont"> 
          <span class="sub">Please Contact:</span> <a href="mailto:medequipment@ribbn.in"> medequipment@ribbn.in </a>  and let us help you choose the right equipment.
      </p> -->

  </div>
  <div class="col-md-3 col-sm-12">

      <!-- <div class="col-sm-3 no-padding-right no-padding-left"> -->
          <button class="thum-btn" type="button" data-toggle="modal" data-target="#requestANurse"> 
            <img src="images/Pointing-Finger.svg" class="pointingFingerClass"> <span class="pointingFingerSpanClass margin-left-min-10" > Request a Home Nurse </span>
            <!-- style="margin-right: auto; margin-left: -30px;" -->
          </button>
      <!-- </div> -->

      <a href="home-care-palliative.php">
        <div class="each-article-thumb">
            <div class="second-overlay"> </div>
            <img src="images/smallthumb-three.jpg">
            <div class="art-thumb-overlay">
                <span class="top-title"> 
                DID YOU KNOW
                </span>
                <p> 
                  Hospice care is only for patients who are no longer receiving curative treatments for their illness, and want to focus only on quality of life.
                </p>
            </div>
        </div>
      </a>
      <a href="concierge.php">
        <div class="each-article-thumb read-also">
            <div class="second-overlay"> </div>
            <img src="images/hospital_discharge.jpg">
            <!-- <img src="images/hospital_discharge.jpg" class="img-responsive" style="height: 197px;" width="100%" alt=""/> -->
            <div class="art-thumb-overlay nurse-read-more">
                <span class="top-title"> 
                ALSO READ
                </span>
                <p> 
                    Finding a vehicle to take you home on the day of your discharge can be a harrowing experience!
                </p>
            </div>
        </div>
      </a>
  </div>

   <p class="med-article-title"> 
      <span>  
      OTHER SERVICES
      </span>
  </p>
  <div class="bottom-icon-wrapper">
    <a href="concierge.php">
      <div class="each-buttom-icon">
          <img class="reverse-image" src="images/AMBULANCE.svg">
          <p> AMBULANCE </p>
      </div>
    </a>
    <a href="medical-equipments.php">
      <div class="each-buttom-icon">
          <img src="images/medicines.svg">
          <p> MED EQUIPMENT </p>
      </div>
    </a>
  </div>
</div>
<?php  include("includes/modals.php"); ?>
<footer class="privacy_footer">
<!--   <div class="container-fluid">
    <div class="row" style="height: 60px">
       <div class="col-md-4 col-sm-12 col-xs-12 text-center" style="display: flex; justify-content: center;">
          <p style="color: black; text-align: justify;"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.<br><span class="rights">All Rights Reserved.</span></p>
       </div>
       <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="social_icons">
             <a href="https://www.facebook.com/Ribbnhealth/"><img src="images/facebook-logo.svg" style="width: 30px; height: 30px; margin-left: 10px"></a>
             <a href="https://www.linkedin.com/company/ribbn/"><img src="images/linkedin.svg" style="width: 30px; height: 30px; margin-left: 20px"></a>
             <a href="https://twitter.com/RibbnHealth"><img src="images/twitter-social.svg" style="width: 30px; height: 30px; margin-left: 20px"></a>
          </div>
       </div>
       <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv">
          <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">Privacy Policy</a>
          <a href="termsofservice.php" style="color: #2f3790; font-size: 14px; margin-right:5px" class="terms">Terms of Service</a>
          <span style="font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>
       </div>
    </div>
  </div> -->

              <div class="row footer_class">
               <div class="col-md-4 col-sm-12 col-xs-12 text-center copyright_class">
                  <p class="copyright_p for-desk-top"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>

                  <p class="copyright_p for-mobile"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="social_icons">
                     <a target="_blank" rel="noopener" href="https://www.facebook.com/Ribbnhealth/" class="icon_class"><i class="fa fa-facebook"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://www.linkedin.com/company/ribbn/" class="icon_class"><i class="fa fa-linkedin"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://twitter.com/RibbnHealth" class="icon_class"><i class="fa fa-twitter"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="blog-headings.php" class="icon_class">
                        <i class="fa fa-rss-square" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv" style="order: -1">
                  <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">PRIVACY POLICY</a>
                  <a href="termsofservice.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:-1em" class="terms">TERMS OF SERVICE</a>
               </div>
               <!-- <span style="order:5; font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>-->
            </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
<script src="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/js/bootstrap.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
        $('#trigger').click(function(event) {
            event.stopPropagation();
            $('#navbar-primary-collapse').toggle();

            $(document).click(function() {
                $('#navbar-primary-collapse').hide();
            });

        });
    });
</script>

<script type="text/javascript">
  $(document).ready(function() {
      function parse_query_string(query) {
        var vars = query.split("&");
        var query_string = {};
        for (var i = 0; i < vars.length; i++) {
          var pair = vars[i].split("=");
          // If first entry with this name
          if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
          } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
          } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
          }
        }
        return query_string;
      };
  });

  $(window).scroll(function() {
        if ($(window).width() > 500) {
            if ($(this).scrollTop() > 5) {
                $(".navbar-me").addClass("fixed-me");
                $('nav').addClass('shrink');
                $('.oldlogo').hide();
                $('.new-logo').show();
            } else {
                $(".navbar-me").removeClass("fixed-me");
                $('nav').removeClass('shrink');
                $('.oldlogo').show();
                $('.new-logo').hide();
            }
        }
    });
</script>

<script>

       $('.modal-checkbox').click(function() {
        if ($(this).is(':checked')) {
           $(this).parents('form').find('.modal-submit-btn').removeAttr('disabled');
        } else {
           $(this).parents('form').find('.modal-submit-btn').attr('disabled', 'disabled');
        }
    });

     $('#trigger').click(function(){

          $(this).parent().next('.navbar-collapse').toggleClass('display-blck');

      });
</script>
<?php include("includes/footer.php"); ?>


</body>
</html>
