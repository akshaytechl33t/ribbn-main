<?php $pageTitle = 'Welcome to Ribbn';   
   include("includes/header.php");
   include("includes/connection.php");
   ini_set('display_errors', 1);
   ini_set('display_startup_errors', 1);
   error_reporting(E_ALL);

   /* Get the details for the requested Table. */
   function getData($table, $limit = false, $link) {
      $data = array();
      if($table == 'registration' || $table == 'referp') {
         $order = "ORDER BY id DESC ";
      } else {
         $order = "ORDER BY is_responded,id DESC ";
      }
      if($limit) {
         $result  = mysqli_query($link, "SELECT * FROM $table $order LIMIT 0,$limit");
      } else {
         $result  = mysqli_query($link, "SELECT * FROM $table $order");
      }
      if($result) {
         while($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
         }
      }
      return $data;
   }
   $q = isset($_GET['q']) ? $_GET['q'] : 'contact';
   if($q) {
      switch($q) {
         case 'hospital':
            $hospitals  = getData('homecare', 0, $link);
         break;

         case 'ambulance':
            $ambulance  = getData('ambulance', 0, $link);
         break;
         
         case 'medical':
            $medical    = getData('medicalsupply', 0, $link);
         break;
         
         case 'patient':
            $patient    = getData('referp', 0, $link);
         break;
         
         case 'reg':
            $reg        = getData('registration', 0, $link);
         break;
         
         case 'contact':
            $contact    = getData('messages', 0, $link);
         break;

         case 'serviceRequest':
            $serviceRequest    = getData('serviceRequest', 0, $link);
         break;
      }
   } else {
      $hospitals        = getData('homecare', 5, $link);
      $ambulance        = getData('ambulance', 5, $link);
      $medical          = getData('medicalsupply', 5, $link);
      $patient          = getData('referp', 5, $link);
      $reg              = getData('registration', 5, $link);
      $contact          = getData('messages', 5, $link);
      $serviceRequest   = getData('serviceRequest', 5, $link);
   }  
?>

<style type="text/css">
   .table1 {
  display: none;
}

#buttondivs button {
   background-color: #2f3790;
   padding: 0.5em 1em;
   border-radius: 5px;
   color: black;
}

#buttondivs a {
   color: white;
   text-decoration: none;
}

#buttondivs {
   margin: 0 25px;
}
#buttondivs a button {
   color:white;
}


</style>


<script>
   function changeStatus(id, type) {
      var inpId = type+'_'+id; 
      var value = $('#'+inpId).val();
      var note  = $('#note_'+inpId).val();
      var qs = 'type='+type+'&id='+id+'&value='+value;
      if(note) {
         qs += '&note='+note;
      }
      $.ajax({
         url: "changeStatus.php",
         type: 'post',
         data: qs , 
         success: function(result){
            if(result == 1) {
               $('#successMsg').removeClass('hide');
               setTimeout(function() {
                  $('#successMsg').addClass('hide');            
               }, 5000);
            } else {
               $('#errorMsg').removeClass('hide');
               setTimeout(function() {
                  $('#errorMsg').addClass('hide');            
               }, 5000)
            }
         }
      });
   }

   function closeAlert(id) {
      $('#'+id).addClass('hide');
   }
</script>
<div class="container">
   <div class="alert alert-success hide" id="successMsg" role="alert">
      Status changed successfully.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="closeAlert('successMsg')">
         <span aria-hidden="true">&times;</span>
      </button>
   </div>

   <div class="alert alert-danger hide" id="errorMsg" role="alert">
      There is some network problem, Please try after sometime.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="closeAlert('errorMsg')">
         <span aria-hidden="true">&times;</span>
      </button>
   </div>
</div>


<div id="fh5co-records">
<section class="navbar-info" id="homepage">
   <div class="fh5co-cover fh5co-cover-style-2 js-full-height" data-stellar-background-ratio="0.5" data-next="yes">
      <div class="container text-center">

      <div style="padding: 4em 0" id="buttondivs">
         <a href="records.php?q=contact" style="float: left;"><button data-table="contactTable" class="tblButn">Contact Us</button></a>
         <a href="records.php?q=patient" style="float: left;"><button data-table="referralTable" class="tblButn">Patient Referrals</button></a>
         <a href="records.php?q=hospital" style="float: left;"><button data-table="hospitalTable" class="tblButn"> Homecare</button></a>
         <a href="records.php?q=ambulance" style="float: left;"><button data-table="ambulanceTable" class="tblButn">Ambulance</button></a>
         <a href="records.php?q=medical" style="float: left;"><button data-table="MesTable" class="tblButn">Medical Equipment</button></a>
         <a href="records.php?q=reg" style="float: left;"><button data-table="registrationTable" class="tblButn">Hospital Registration</button></a>
         <a href="records.php?q=serviceRequest" style="float: left;"><button data-table="ServiceRequestTable" class="tblButn">Service Request</button></a>
      </div>
         
         <?php if(!$q || $q == 'hospital') { ?>
            <h2>Homecare</h2>
            <table class="table table-striped table-bordered table_records tablenumber" id="hospitalTable">
               <thead >
                  <tr style="text-align: left;">
                     <th>Sl.no</th>
                     <th>Name</th>
                     <th>Email</th>
                     <th>Mobile</th>
                     <th>Date</th>
                     <th>Notes</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     if(count($hospitals) > 0) {
                        $i=1;
                        foreach ($hospitals as $val) {
                           print '<tr>
                              <td>'.$i.'</td>
                              <td>'.$val['homecare_name'].'</td>
                              <td>'.$val['homecare_email'].'</td>
                              <td>'.$val['homecare_contact'].'</td>
                              <td>'.date('D, d M Y', strtotime(@$val['created_at'])).'</td>
                              <td style="max-width: 100px; overflow: hidden;"><textarea id="note_hospital_'.$val['id'].'" style="width: 100%;">'.@$val['note'].'</textarea></td>
                              <td style="text-align:center">
                                 <select id="hospital_'.$val['id'].'" class="selectRespond">
                                    <option value="0">New Mail</option>
                                    <option value="1"';
                                    if(isset($val['is_responded'])) {
                                 if ($val['is_responded'] == 1) { echo 'selected';}} 
                                    print '>Responded</option>
                                 </select>
                              </td>
                              <td style="text-align:center" class="respondMails">
                                 <button class="btn btn-sm" onclick="changeStatus('.$val['id'].', \'hospital\')">Submit</button>
                              </td>
                           </tr>';
                           $i++;
                        }
                        
                     } else {
                        print '<tr colspan="7">No Records Found.</tr>';
                     }
                  ?>
               </tbody>
            </table>
            
         <?php } if(!$q || $q == 'ambulance') { ?>
            <h2 class="table_head">Ambulance</h2>            
            <table class="table table-striped table-bordered table_records tablenumber" id="ambulanceTable">
               <thead >
                  <tr style="text-align: left;">
                     <th>S.no</th>
                     <th>Name</th>
                     <th>Email</th>
                     <th>Mobile</th>
                     <th>Date</th>
                     <th>Notes</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     if(count($ambulance) > 0) {
                        $i=1;
                        foreach ($ambulance as $val) {
                           print '<tr>
                              <td>'.$i.'</td>
                              <td>'.$val['ambulance_name'].'</td>
                              <td>'.$val['ambulance_email'].'</td>
                              <td>'.$val['ambulance_contact'].'</td>
                              <td>'.date('D, d M Y', strtotime(@$val['created_at'])).'</td>
                              <td style="max-width: 100px; overflow: hidden;"><textarea id="note_ambulance_'.$val['id'].'" style="width: 100%;">'.@$val['note'].'</textarea></td>
                              <td style="text-align:center">
                                 <select id="ambulance_'.$val['id'].'" class="selectRespond">
                                    <option value="0">New Mail</option>
                                    <option value="1"';
                                    if(isset($val['is_responded'])) {
                                    if ($val['is_responded'] == 1) { echo 'selected';}} 
                                    print '>Responded</option>
                                 </select>
                              </td>
                              <td style="text-align:center" class="respondMails">
                                 <button class="btn btn-sm" onclick="changeStatus('.$val['id'].', \'ambulance\')">Submit</button>
                              </td>
                           </tr>';
                           $i++;
                        }
                        
                     } else {
                        print '<tr colspan="6">No Records Found.</tr>';
                     }
                  ?>
               </tbody>
            </table>

         <?php } if(!$q || $q == 'medical') { ?>
            <h2 class="table_head">Medical Equipment Supplier</h2>            
            <table class="table table-striped table-bordered table_records tablenumber" id="MesTable">
               <thead >
                  <tr style="text-align: left;">
                     <th>S.no</th>
                     <th>Name</th>
                     <th>Email</th>
                     <th>Mobile</th>
                     <th>Type</th>
                     <th>Date</th>
                     <th>Notes</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     if(count($medical) > 0) {
                        $i=1;
                        foreach ($medical as $val) {
                           print '<tr>
                              <td>'.$i.'</td>
                              <td>'.$val['medical_name'].'</td>
                              <td>'.$val['medical_email'].'</td>
                              <td>'.$val['medical_contact'].'</td>
                              <td>'.$val['medical_select'].'</td>
                              <td>'.date('D, d M Y', strtotime(@$val['created_at'])).'</td>
                              <td style="max-width: 100px; overflow: hidden;"><textarea id="note_medical_'.$val['id'].'" style="width: 100%;">'.@$val['note'].'</textarea></td>
                              <td style="text-align:center">
                                 <select id="medical_'.$val['id'].'" class="selectRespond">
                                    <option value="0">New Mail</option>
                                    <option value="1"';
                                    if(isset($val['is_responded'])) {
                                    if ($val['is_responded'] == 1) { echo 'selected';}} 
                                    print '>Responded</option>
                                 </select>
                              </td>
                              <td style="text-align:center" class="respondMails">
                                 <button class="btn btn-sm" onclick="changeStatus('.$val['id'].', \'medical\')">Submit</button>
                              </td>
                           </tr>';
                           $i++;
                        }
                        
                     } else {
                        print '<tr colspan="6">No Records Found.</tr>';
                     }
                  ?>
               </tbody>
            </table>

         <?php } if(!$q || $q == 'patient') { ?>
            <h2 class="table_head">Patient Referrals</h2>            
            <table class="table table-striped table-bordered table_records table tablenumber" id="referralTable">
               <thead >
                  <tr style="text-align: left;">
                     <th>S.no</th>
                     <th>Referrer Name</th>
                     <th>Referrer Mobile</th>
                     <th>Patient Name</th>
                     <th>Patient Mobile</th>
                     <th>Date</th>
                     <th>Notes</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     if(count($patient) > 0) {
                        $i=1;
                        foreach ($patient as $val) {
                           print '<tr>
                              <td>'.$i.'</td>
                              <td>'.$val['referrer_name'].'</td>
                              <td>'.$val['referrer_number'].'</td>
                              <td>'.$val['patient_name'].'</td>
                              <td>'.$val['patient_number'].'</td>
                              <td>'.date('D, d M Y', strtotime(@$val['created_at'])).'</td>
                              <td style="max-width: 100px; overflow: hidden;"><textarea id="note_patient_'.$val['id'].'" style="width: 100%;">'.@$val['note'].'</textarea></td>
                              <td style="text-align:center" class="respondMails">
                                 <button class="btn btn-sm" onclick="changeStatus('.$val['id'].', \'patient\')">Submit</button>
                              </td>
                           </tr>';
                           $i++;
                        }
                        
                     } else {
                        print '<tr colspan="6">No Records Found.</tr>';
                     }
                  ?>
               </tbody>
            </table>

         <?php } if(!$q || $q == 'reg') { ?>

            <h2 class="table_head">Hospital Registration</h2>            
            <table class="table table-striped table-bordered table_records tablenumber" id="registrationTable">
               <thead >
                  <tr style="text-align: left;">
                     <th>S.no</th>
                     <th>Hospital</th>
                     <th>Contact Name</th>
                     <th>Contact Number</th>
                     <th>Date</th>
                     <th>Notes</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     if(count($reg) > 0) {
                        $i=1;
                        foreach ($reg as $val) {
                           print '<tr>
                              <td>'.$i.'</td>
                              <td>'.$val['reg_hospital'].'</td>
                              <td>'.$val['reg_name'].'</td>
                              <td>'.$val['reg_contact'].'</td>
                              <td>'.date('D, d M Y', strtotime(@$val['created_at'])).'</td>
                              <td style="max-width: 100px; overflow: hidden;"><textarea id="note_reg_'.$val['id'].'" style="width: 100%;">'.@$val['note'].'</textarea></td>
                           <td style="text-align:center" class="respondMails">
                                 <button class="btn btn-sm" onclick="changeStatus('.$val['id'].', \'reg\')">Submit</button>
                              </td></tr>';
                           $i++;
                        }
                        
                     } else {
                        print '<tr colspan="5">No Records Found.</tr>';
                     }
                  ?>
               </tbody>
            </table>
         
         <?php } if(!$q || $q == 'serviceRequest') { ?>
            <h2 class="table_head">Service Requests</h2>            
            <table class="table table-striped table-bordered table_records tablenumber" id="registrationTable">
               <thead >
                  <tr style="text-align: left;">
                     <th>S.no</th>
                     <th>Service Type</th>
                     <th>Contact Name</th>
                     <th>Contact Number</th>
                     <th>Date</th>
                     <th>Email</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     if(count($serviceRequest) > 0) {
                        $i=1;
                        foreach ($serviceRequest as $val) {
                           print '<tr>
                              <td>'.$i.'</td>
                              <td>'.$val['serviceType'].'</td>
                              <td>'.$val['contact_name'].'</td>
                              <td>'.$val['contact_number'].'</td>
                              <td>'.date('D, d M Y', strtotime(@$val['created_at'])).'</td>
                              <td>'.$val['contact_email'].'</td>
                              <td style="text-align:center">
                                 <select id="serviceRequest_'.$val['id'].'" class="selectRespond">
                                    <option value="0">New Mail</option>
                                    <option value="1"';
                                    if(isset($val['is_responded'])) {
                                 if ($val['is_responded'] == 1) { echo 'selected';}} 
                                    print '>Responded</option>
                                 </select>
                              </td>

                           <td style="text-align:center" class="respondMails">
                                 <button class="btn btn-sm" onclick="changeStatus('.$val['id'].', \'serviceRequest\')">Submit</button>
                              </td></tr>';
                           $i++;
                        }
                        
                     } else {
                        print '<tr colspan="5">No Records Found.</tr>';
                     }
                  ?>
               </tbody>
            </table>
         

         <?php } if(!$q || $q == 'contact') { ?>

            <h2>Contact Us</h2>            
            <table class="table table-striped table-bordered table_records tablenumber" id="contactTable">
               <thead >
                  <tr style="text-align: left;">
                     <th>S.no</th>
                     <th>Email</th>
                     <th>Date</th>
                     <th>Message</th>
                     <th>Notes</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     if(count($contact) > 0) {
                        $i=1;
                        // echo "<pre>"; print_r($contact); 
                        foreach ($contact as $val) {
                           print '<tr>
                              <td>'.$i.'</td>
                              <td>'.$val['name'].'</td>
                              <td>'.date('D, d M Y', strtotime(@$val['created_at'])).'</td>
                              <td>'.$val['message'].'</td>
                              <td style="max-width: 100px; overflow: hidden;"><textarea id="note_contact_us_'.$val['id'].'" style="width: 100%;">'.@$val['note'].'</textarea></td>
                              <td style="text-align:center">
                                 <select id="contact_us_'.$val['id'].'" class="selectRespond">
                                    <option value="0">New Mail</option>
                                    <option value="1"';
                                    if(isset($val['is_responded'])) {
                                    if ($val['is_responded'] == 1) { echo 'selected';}} 
                                    print '>Responded</option>
                                 </select>
                              </td>
                              <td style="text-align:center" class="respondMails">
                                 <button class="btn btn-sm" onclick="changeStatus('.$val['id'].', \'contact_us\')">Submit</button>
                              </td>
                           </tr>';
                           $i++;
                        }
                        
                     } else {
                        print '<tr colspan="5">No Records Found.</tr>';
                     }
                  ?>
               </tbody>
            </table>
         
         <?php } ?>
      </div>
</section>
</div>



<script type="text/javascript">
   $(document).ready(function() {
      $('.respondMails').on('click',function(){
         if($(this).parents('tr').find('.selectRespond').val() == 1) {
            $(this).parents('tr').find('.selectRespond option:eq(1)').attr('selected', 'selected');
            $(this).parents('tr').find('.selectRespond').prop('disabled',true);
            var trHtml = $(this).parents('tr').clone();
            //selectRespond
            $(this).parents('tbody').append(trHtml);
            $(this).parents('tr').remove();
            var ii = 1;
             $('.tablenumber').find('tbody').find('tr').each(function(){
               var curr = this;
               $(curr).find('td:first').html(ii);
                  ii++;
             })
         }
      });
      $('.respondMails').trigger('click');
      $('.tblButn').click(function() {
         var tableName = $(this).attr('data-table');
         $('.table1').hide();
         $("#"+tableName).show();
      });
   });

</script>






</body>
</html>
