<?php  $pageTitle = 'Privacy Policy'; include("includes/header.php"); ?>
<title>Privacy Policy</title>
<nav class="navbar navbar-default navbar-me" style="background-color: white">
    <div class="container-fluid "> 
      <div class="navbar-header">
        <button id="trigger" type="button" class="navbar-toggle collapsed menu-collapsed-button" data-toggle="collapse" data-target="#navbar-primary-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
 <a class="navbar-brand site-logo" href="index.php" id="home" ><img id="newlogo" data-target="index.php" src="images/ribbn_newlogo2x.png" width="150" height="50" alt="" style="padding-top: 0.2em; padding-bottom: 0.2em" /></a> </div>
           <div class="collapse navbar-collapse " id="navbar-primary-collapse" style="box-shadow: 0px 5px 5px -5px grey">
              <ul class="nav navbar-nav">
                <li> <a id="menu_first_link" class="menu_links" href="index.php#whoweare" style="font-weight: 600;">WHO WE ARE</a> </li>
                <li class="margin-Top10">.</li>
                <li> <a class="menu_links" href="index.php#whatwedo" style="font-weight: 600;">WHAT WE DO</a> </li>
                <li class="margin-Top10">.</li>
                <li> <a class="menu_links" href="index.php#patients" style="font-weight: 600;">WHAT WE OFFER</a> </li>
                <li class="margin-Top10">.</li>
                <li> <a class="menu_links" href="index.php#referapatients" style="font-weight: 600;">REFER A PATIENT</a> </li>
                <li class="margin-Top10">.</li>
                <li> <a class="menu_links" href="index.php#partners" style="font-weight: 600;">PARTNER WITH US</a> </li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li>
                <a href="index.php#haveQuery" class="menu_links" data-target="index.php#haveQuery" style="font-weight: 600; padding-top: 18px; padding-bottom: 1.5rem">Contact Us</a>
                </li>
                <li class="margin-Top-sr"></li>
                </ul>
            </div>
        </div>
    </nav>

	<div class="container" style="background-color: #fff; margin-top: 70px; color: black; font-size: 14px; margin-bottom: -60px" id="privacy_cont">

		<h3 class="text-center privacy_header" style="text-decoration: underline; color: #2f3790;">Ribbn - Privacy Policy</h3>
<p style="color: black; text-align: left">This privacy notice discloses the privacy practices for <a href="index.php" style="color:#2f3790">www.ribbn.in</a>. This notice applies solely to information collected by this website. It will notify you of the following:<br>
	<ol>
		<li>What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.</li>
		<li>The choices available to you regarding the use of your data.</li>
		<li>The security practices and/ or procedures in place to protect your information.</li>
		<li>How you can update your information and/ or correct any inaccuracies in the information.</li><br>
	</ol>
Ribbn is committed to maintaining your confidence and trust, and accordingly maintains the following privacy policy to protect your personal information.<br><br>
<b>Personal Information that Ribbn May Collect</b><br><br>
<b>Registration</b> <br><br>
In order to use the services mentioned on this website, a user must first complete the registration form. Ribbn collects the following user information, including but not limited to personal details viz., name, age, date of birth, postal and/ email address, phone number and other contact details, health records, discharge summary, employment details, banking information, insurance coverage details). Ribbn also collects business information which may include business details viz., Entity Name & registration details, Financials, Bank Account Details, from sole proprietorships, and registered entities that signup for partnering with Ribbn to provide services to Ribbn’s customers.<br><br>


We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us while registering and/ or contacting us through the forms on this website and/ or via email or other direct contact from you. We will not sell or rent this information to anyone. We will use your information to respond to you, regarding the reason you contacted us. We partner with other parties/ vendors/ service providers to provide specific services. When the user signs up for these services, we will share names, or other contact information that is necessary for the third party to provide these services. These parties are not allowed to use personally identifiable information except for the purpose of providing these services. <br><br>

<b>Ribbn’s Commitment to Security</b><br><br>
We take precautions to protect your information. When you submit sensitive information via the website and other means your information is protected both online and offline.<br><br>

Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for "https" at the beginning of the address of the web page.<br><br>

While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.<br><br>

All Ribbn employees, partners, agents and contractors with access to personal information are also bound to adhere to this policy as part of their contract with Ribbn.<br><br>

<b>How Ribbn May Use Personal Information</b><br><br>
Unless you consent, Ribbn will use your personal information for the purpose for which it was submitted such as to providing you concierge services, and/ or home care services; the information collected will be used in recordkeeping and to conduct research on industry marketplace practices, which research may lead to the publication of aggregate demographic data but will not result in the reporting or publication of any personal information provided to us. At any time you can add or remove your name from our mailing list by contacting us at <a style="color:#2f3790" href="mailto:contact@ribbn.in">contact@ribbn.in.</a><br><br> 
<b>How Your Information May Be Shared</b><br><br>
In order to process your requests, Ribbn will transfer your request along with your personal information (as may be needed) to our service providers/ vendors/ partners<br><br>

We will also release personal information under the following circumstances:<br>
<ul>
	<li>
		Where release is required by law (for example, a subpoena) or regulation, or is requested by an agency for governmental investigative purposes or governmental proceedings;
	</li>
	<li>
		Where our records indicate a company may be engaged in fraudulent activity or other deceptive practices that a governmental agency should be made aware of; or
	</li>
	<li>
		To appropriate persons, where your communication suggests possible harm to others.		
	</li>
</ul>
<br>


<b>Your Access to and Control Over Your Information</b><br><br>

Unless you ask us not to, we may contact you via email or SMS in the future to tell you about our programs and offerings, new products or services, or changes to this privacy policy.<br><br>

You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:<br><br>
<ul>
	<li>See what data we have about you, if any.</li>
	<li>Change/correct any data we have about you.</li>
	<li>Have us delete any data we have about you.</li>
	<li>Express any concern you have about our use of your data.</li>
</ul>
<br>

<b>Help Us Keep Your Personal Information Accurate</b><br><br>
If your personal information changes or you would like to review the personal information we may have on file, please send an email to <a style="color:#2f3790" href="mailto:contact@ribbn.in">contact@ribbn.in</a>. At any time you can add or remove your name from our mailing list by contacting us at <a style="color:#2f3790" href="mailto:contact@ribbn.in">contact@ribbn.in</a>.<br><br>
<b>Computer Tracking and Cookies</b><br><br>
Our web site is not set up to track, collect or distribute personal information not entered by its visitors. Our site logs do generate certain kinds of non-identifying site usage data, such as the number of hits and visits to our site. This information is used for internal purposes by technical support staff to provide better services to the public and may also be provided to others, but again, the statistics contain no personal information and cannot be used to gather such information.<br><br>

A cookie is a small amount of data that is sent to your browser from a Web server and stored on your computer's hard drive. Ribbn uses non-identifying cookies to provide easier site navigation and access to forms. You can still use Ribbn site if your browser is set to reject cookies. Our cookies do not generate personal data, do not read personal data from your machine and are never tied to anything that could be used to identify you.<br><br>
<b>Links</b> <br><br>
This website contains links to other sites. Please be aware that we are not responsible for the content or privacy practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of any other site that collects personally identifiable information.<br><br>
<b>Problems</b><br><br>
If you have a complaint about Ribbn’s compliance with this privacy policy, you may contact us at <a href="mailto:contact@ribbn.in" style="color:#2f3790">contact@ribbn.in.</a> 
</p>
<br><br>
                    <!--  <a class="privacy_backtotop" alt="Back To Top" href="#privacy_cont" style="position: relative; bottom: 20px"><img style="width:35px; height: 35px" src="images/top_arrow.svg"></a> -->

</div>

<a class="privacy_backtotop" alt="Back To Top" href="#privacy_cont"><img style="width:50px; height: 50px" src="images/top_arrow.svg"></a>


<footer class="privacy_footer">
              <div class="row footer_class">
               <div class="col-md-4 col-sm-12 col-xs-12 text-center copyright_class">
                  <p class="copyright_p""><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="social_icons">
                     <a target="_blank" rel="noopener" href="https://www.facebook.com/Ribbnhealth/" class="icon_class"><i class="fa fa-facebook"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://www.linkedin.com/company/ribbn/" class="icon_class"><i class="fa fa-linkedin"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://twitter.com/RibbnHealth" class="icon_class"><i class="fa fa-twitter"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="blog-headings.php" class="icon_class">
                        <i class="fa fa-rss-square" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv" style="order: -1">
                  <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">PRIVACY POLICY</a>
                  <a href="termsofservice.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:-1em" class="terms">TERMS OF SERVICE</a>
               </div>
               <!-- <span style="order:5; font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>-->
            </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script type="text/javascript">
      $(document).ready(function() {
        $('#trigger').click(function(event) {
            event.stopPropagation();
            $('#navbar-primary-collapse').toggle();

            $(document).click(function() {
                $('#navbar-primary-collapse').hide();
            });

        });
    });
</script>
<script type="text/javascript">
  $(document).ready( function (event) {
    $('.privacy_backtotop').click(function(){
      $('html, body').animate({
          scrollTop: $( "#privacy_cont" ).offset().top
      }, 500);
      return false;
    });
});
</script>

</body>
</html>
