<?php include("header.php"); ?>
<nav class="navbar navbar-default navbar-me">
   <div class="container-fluid ">
      <div class="navbar-header">
         <button id="trigger" type="button" class="navbar-toggle collapsed menu-collapsed-button" data-toggle="collapse" data-target="#navbar-primary-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
         <a class="navbar-brand site-logo oldlogo" href="#fh5co-page" id="home"><img src="images/logo.png" data-target="#fh5co-page" class="" width="103" height="165" alt=""/></a> <a class="navbar-brand site-logo new-logo" href="#fh5co-page" id="home"><img id="newlogo" data-target="#fh5co-page" src="images/sticky-logo-2.png" width="190" height="72" alt=""/></a> 
      </div>
      <div class="collapse navbar-collapse " id="navbar-primary-collapse">
         <ul class="nav navbar-nav">
            <li> <a class="" href="#whoweare" style="font-weight: 600;">WHO WE ARE</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="" href="#whatwedo" style="font-weight: 600;">WHAT WE DO</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="" href="#patients" style="font-weight: 600;">WHAT WE OFFER</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="" href="#referapatients" style="font-weight: 600;">REFER A PATIENT</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="" href="#partners" style="font-weight: 600;">PARTNER WITH US</a> </li>
         </ul>
         <ul class="nav navbar-nav navbar-right">
            <li>
               <a href="#haveQuery" data-target="#haveQuery" style="font-weight: 600; padding-top: 18px;">Contact Us</a>
            </li>
            <li class="margin-Top-sr"></li>
         </ul>
      </div>
   </div>
</nav>
<div class="container" style="background-color: #eee">
<h3 class="text-center" style="padding-top: 5%; color: #2f3790; text-decoration: underline;">Terms Of Service</h3>
<p>
   Acceptance of the Terms of Use<br><br>
   Welcome to ribbn.in, the website of <a href="../index.php" style="color: #2f3790">Ribbn.in</a> Healthcare Management Services, a company dedicated to providing healthcare management services, (hereinafter referred to in this document as "Ribbn.in", “ribbn” or "us", "we" or "our"). The following terms and conditions, (“Terms of Use”), apply to your use of the <a href="../index.php" style="color: #2f3790">www.ribbn.in</a> website, and the content. Please read these Terms of Use carefully before using the website. By using the website, you accept and agree to abide by these Terms of Use. Do not use this website if you do not agree to these Terms of Use.<br><br>
   Amendments and Notice<br><br>
   We may, from time to time, at our sole discretion and without advance notice, amend these Terms of Use. Please visit this page regularly to make yourself aware of the changes as they are binding on you.<br><br>
   Location<br><br>
   The website is intended for use in India.<br><br>
   Disclaimer<br><br>
   Ribbn Care Management Services is not providing medical advice or medical service(s) in any manner whatsoever. All of the content on this website is intended for information purposes only. It is not be used for medical diagnosis, treatment or as a substitute for professional services of qualified medical and / or health professionals. Always seek the advice of your Doctor and / or your professional medical or health adviser before making any changes to your healthcare plans.<br><br>
   Ownership and Intellectual Property Rights<br><br>
   The entire contents of the website (including, text, images, pictures, videos, audio files, functionality and the URL, <a href="../index.php" style="color: #2f3790">www.ribbn.in</a>,  (collectively the “Content”), are the property of Ribbn Healthcare Management Services, and are protected by International copyright, patents, copyright, trademarks, and other intellectual property (IP) laws. Your use of this website and the information herein is permitted for your personal, noncommercial use only; if you are associated with Us as a customer, provider or partner the use of the website and its contents herein is permitted for legitimate business purposes and as laid out in the terms of the agreement(s). <br><br>
   Your use of this website and its content herein does not grant you the right to reproduce, sell or exploit for any commercial purposes any part of the website and the content within, or use the website or any of its content without obtaining express permission or license to do so from Us. <br><br>
   Any act of or attempt to breach the above terms, including, but not limited to, printing, copying, or otherwise use any part of the website, will result in immediate termination of your right to use the website. No right, title or interest in or to the website and the content within are transferred to you, and any rights not expressly granted are reserved by Us. Any use of the website not expressly permitted by these Terms of Use is a breach of these Terms of Use and may violate copyright, trademark and other laws.<br><br>
   Accessing the Website<br><br>
   To use this website, you must be of a legal age as defined in the law of the state and in any case not less than 18 (eighteen) years. If you do not meet this requirement, kindly exit this website immediately. Access to the website is permitted on a temporary basis, and we reserve the right to withdraw or amend any service we provide on the website at our sole discretion without notice. We will not be liable if all or any part of the website is unavailable at any time or for any period. <br><br>
   We collect and use information, including but not limited to, details of the device and the IP address from where you access the website, in accordance with our 
   <Privacy Policy>
   . By accessing the website, you consent to such collection and use.<br><br>
   Access to the website or some sections within may require you to register once and thereafter use the credentials provided to you, to login to the website, especially sections to which access is granted. It is your responsibility to ensure that all the information you provide is, correct, complete and up to date. Additionally it is your responsibility to safeguard the credentials provided to you to login to the services. By providing the information you agree to receive information from time to time from Ribbn. Notwithstanding any of the above, if we believe that an user is not compliant to the Terms of Use, Ribbn.in reserves the right to disable and terminate access to the registered user, without cause or notice. Please review the 
   <Privacy Policy>
   for more details.<br><br>
   User Information<br><br>
   The website contains certain sections where user information, including but not limited to personal details viz., name, age, date of birth, address, and other contact details, health records, discharge summary, employment details, banking information, insurance coverage details, business details viz., Entity Name & registration details, Financials, Bank Account Details, (collectively referred to as “User Information”), is collected. You represent and warrant that such information is accurate and up to date and you must take responsibility to update this to reflect any changes. By providing this information you grant Ribbn.in permission to use and share the same with our partners and service providers.  <br> <br>
   Acceptable Use Policy<br><br>
   Access to and use of this website is only for lawful purposes and in accordance with these Terms of Use. <br><br>
   Links to Third-Party Sites<br><br>
   The website may contain links to, third-party websites not under the control of Ribbn.in. Ribbn.in is not responsible for the content and availability of linked third-party sites. Ribbn.in encourages you to undertake necessary and appropriate due diligence before proceeding with accessing these third party linked sites.<br><br>
   Disclaimer of Warranties With Respect to Website<br><br>
   Ribbn.in does not represent or guarantee the accuracy or reliability of any of the Content, nor the quality of any information or other materials or services displayed, sold, or purchased by you as a result of your use of our website. You acknowledge that any reliance upon the information or materials shall be at your sole risk. Unless otherwise provided in these Terms of Use, Ribbn.in reserves the right, at its discretion, to correct any error or omissions in any portion of our website, with or without notice to you.<br><br>
   NOTWITHSTANDING ANYTHING CONTAINED HEREIN TO THE CONTRARY, THE WEBSITE AND ALL INFORMATION HEREIN IS PROVIDED "AS IS" AND "AS AVAILABLE," WITHOUT ANY WARRANTIES OR CONDITIONS WHATSOEVER, EXPRESSED OR IMPLIED. NOTWITHSTANDING THE FOREGOING OR ANY STATEMENT TO THE CONTRARY CONTAINED IN THESE TERMS OF USE, Ribbn.in DOES NOT WARRANT THAT THE CONTENT WILL MEET YOUR REQUIREMENTS. Ribbn.in HEREBY DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES OF ANY KIND, WHETHER EXPRESS, STATUTORY, OR IMPLIED, ORAL OR WRITTEN, WITH RESPECT TO THE WEBSITE AND ITS CONTENT, INCLUDING WITHOUT LIMITATION ALL IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON‐INFRINGEMENT, ACCURACY, RELIABILITY, COMPLETENESS, OR TIMELINESS, AND ALL WARRANTIES IMPLIED FROM ANY COURSE OF DEALING, COURSE OF PERFORMANCE, OR USAGE OF TRADE.<br><br>
   Limitations of Liability<br><br>
   IN NO EVENT WILL Ribbn.in OR ITS AFFILIATES OR ANY OF THEIR RESPECTIVE EMPLOYEES, OFFICERS, DIRECTORS, SHAREHOLDERS, AGENTS, REPRESENTATIVES, LICENSORS, OR SUPPLIERS BE LIABLE TO YOU OR TO ANY THIRD PARTY FOR ANY DIRECT, SPECIAL, CONSEQUENTIAL, INDIRECT, OR INCIDENTAL LOSSES, DAMAGES, OR EXPENSES, DIRECTLY OR INDIRECTLY RELATING TO THE USE OR MISUSE OF OUR WEBSITE, OR WITH RESPECT TO ANY OTHER HYPERLINKED WEBSITE, OR THE CONTENT USED HEREIN OR THEREWITH, WHETHER IN AN ACTION IN CONTRACT, TORT, STRICT LIABILITY, NEGLIGENCE, OR OTHERWISE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. <br><br>
   Indemnification<br><br>
   You agree to indemnify and hold harmless Ribbn.in, its subsidiaries, affiliates, directors, officers, agents, vendors or other partners, and employees, from any claim or demand, including attorneys' fees, due to or arising out of (i) your breach of this Terms of Use, or your violation of any law or the rights of a third party; or (ii) any material or information posted, provided, transmitted, or otherwise made available by you on the website.<br><br>
   Governing Law<br><br>
   These Terms of Use shall be governed by the laws of India where these services are intended to be provided. <br><br>
   This Terms of Use, and any additional terms referenced herein, constitute the entire Terms of Use between Ribbn.in and you with respect to the website. If any provision of this Terms of Use is found to be invalid or unenforceable, the remaining provisions shall be enforced to the fullest extent possible, and the remaining Terms of Use shall remain in full force and effect. This Terms of Use inures to the benefit of Ribbn.in, its successors and assigns.<br><br><br><br>
   Please use the <a href="../index.php">Contact Us</a> page for any comments or questions regarding the website.<br><br>
</p>