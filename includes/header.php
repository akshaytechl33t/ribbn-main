<!DOCTYPE html>
<html lang="en">
      <head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112596007-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112596007-1');
</script>

      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="images/favicon.ico" rel="shortcut icon">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title><?php echo $pageTitle ?></title>
      <!-- Google Fonts -->
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700|Monsterrat:400,700|Merriweather:400,300italic,700' rel='stylesheet' type='text/css'>

      <!-- Animate.css -->
      <link rel="stylesheet" href="css/animate.css">
      <link href="css/bootstrap-dropdownhover.min.css" rel="stylesheet">
      <!-- Bootstrap  -->
      <link rel="stylesheet" href="css/bootstrap.css">
      <!-- ffont-awesome -->
      <link rel="stylesheet" href="css/font-awesome-min.css">
      <!-- Cards -->
      <link rel="stylesheet" href="css/ribbnCards.css">
      <!-- Cards -->
      <link rel="stylesheet" href="css/bootstrap-theme.css">
      <link rel="stylesheet" href="css/bootstrap-multiselect.css">
      <link href="css/bootstrap-tabs-x.css" media="all" rel="stylesheet" type="text/css"/>
      <link rel = "stylesheet" href = "css/jquery-ui.css">
      <link rel="stylesheet" type="text/css" href="partner_login.css">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i" rel="stylesheet">
      <link rel="stylesheet" href="css/custom.css">



      <!-- Modernizr JS -->
      <!-- <script src="https://use.fontawesome.com/7aa648423f.js"></script> -->
      <script src="https://use.fontawesome.com/7aa648423f.js"></script>
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
      <script src="js/modernizr-2.6.2.min.js"></script>
      <!-- FOR IE9 below -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
      <!-- jQuery -->

      <script src="js/bootstrap-tabs-x.js" type="text/javascript"></script>
      <script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
      <script>
function changeText()
{
 document.getElementById('boldStuff').innerHTML = 'Fred Flinstone';
}
</script>
<style type="text/css">


#ambulance_name-error, #ambulance_contact-error, #ambulance_email-error, #homecare_name-error, #homecare_contact-error, #homecare_email-error, #medicines_name-error, #medical_contact-error, #medical_email-error, #ref_name-error, #referrer_number-error, #ref_pat_name-error, #patient_number-error, #reg_hospital-error, #primary_cont_name-error, #reg_contact-error, #queryemail-error, #querymessage-error {
  font-size: 12px;
};

.what_we_offer_head {
      color: white;
      width: 200px;
      /*text-align: left;*/
   }

.form-control {
  font-size: 16px!important;
}

.modalclosebtn {
  background-color: white; 
  border: 2px solid #fb4f59;
}


.modalclosebtn:hover {
  color: white;
  background-color: #fb4f59;
}

.referpatients_span {
  position: absolute; 
  bottom: 0; 
  left: 0; 
  right: 0;
}

#patients {
  background-image: linear-gradient(rgba(16, 17, 153, 0.7),rgba(237, 42, 84, 0.7)),url("images/website_background.jpg");
}

.copyright_p {
  color: black; 
  text-align: center; 
  letter-spacing: 0.5px; 
  font-size: 13px;
}

.backtotop_btn {
  position: absolute; 
  right: 50px; 
  bottom: 20px;
}

.referpatients_p {
  font-size: 13px; 
  line-height: 20px; 
  text-align: justify; 
  width: 80%; 
  margin: 0 auto;
}

.privacy_backtotop {
  position: relative;
  left: 95%;
  bottom: 20px;
}

.tos_backtotop {
  position: relative;
  left: 95%;
  bottom: 20px;
}


#know_more_2nd {
  margin-bottom: 50px!important;
}

#partners_p {
    width: 80%;
    margin: 0 auto;
    text-align: center;
}

.partner_login_row .partner_top {
  height: 20%; 
  display: flex; 
  align-items: center; 
  justify-content: flex-start; 
  flex-direction: column;
}

.partner_close {
  align-self: flex-end; 
  padding-right: 15px; 
  font-size: 30px; 
  color: #aaa;
}

.partner_login_row .partner_h4 {
  align-self: center; 
  margin-top: 30px; 
  font-size: 22px;
}

.partner_login_form img {
  margin-top: 0px;
  height: 30px; 
  width: 30px;
}

.partner_login_form .form-check {
  align-self: flex-end; 
  padding-right: 15px;
}

.partner_login_form .form-check a {
  text-decoration: none; 
  font-size: 14px; 
  color: #2f3790;
}

.partner_login_cont .partner_bottom_row {
  height: 20%; 
  width: 100%; 
  margin-left:0; 
  background-color: #2f3790; 
  color: white;
}

.partner_bottom_row p {
  font-size: 12px; 
  width: 75%; 
  margin: 0 auto;
}


h3.haveQueryDiv span { 
  margin-top: -4em;   
  padding-bottom: 4em; 
  display: block; 
}

.partner_login_form {
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 70%;
  justify-content: space-around;
}

.partner_login_form input:-webkit-autofill {
    -webkit-box-shadow: 0 0 0px 1000px white inset;
    -webkit-text-fill-color: blue;
    
}

.partner_login_form input {
  width: 80%;
  margin: 0 auto;
  border: none!important;
  background-color: transparent;
  border-top: none!important;
  border-radius: 0;
  display: inline;
  font-size: 13px;
  font-weight: 500;
  padding: 0;
  padding-left: 10px;
  -webkit-appearance : none;
}

.partner_login_cont {
  width: 25%; 
  margin: 5% auto 10% auto; 
  height: 70vh; 
  padding: 0; 
  box-shadow: 0 9px 0px 0px white, 0 -9px 0px 0px white, 5px 0 5px -4px rgba(31, 73, 125, 0.8), -5px 0 5px -4px rgba(31, 73, 125, 0.8);
}

.partner_login_row {
  height: 80%; 
  width: 100%; 
  background-color: white; 
  margin-left: 0; 
  display: flex; 
  flex-direction: column; 
  justify-content: space-around;
}


.newform:focus {
  border-color: inherit;
  -webkit-box-shadow: none;
  box-shadow: none;
  text-decoration: none;
}

#input_id {
  outline: none!important;
}

.partner_login_form .form-group {
  width: 80%;
}

.partner_login_form .form-group-email {
  border-bottom: 1px solid #ccc;
}

.partner_login_form .submit_btn {
  height: 2.5rem;
  width: 7.5rem;
  background-color: #2f3790;
  color: white;
}

#new_anchor:after {
  background: #333;
    width: 0px;
    height: 1px;
    position: absolute;
    bottom: 0px;
    content: '';
    left: 15px;
    opacity: 0;
}

#new_anchor:hover:after {
  transition: all ease .3s;
}

.icon_class {
  width: 50px;
  height: 50px;
  line-height: 50px;
  background: lightgrey;
  text-align: center;
  color: #7d7d7d;
  border-radius: 50%;
  display: inline-block;
  font-size: 18px;
}


.icon_class:hover .fa-facebook {
  transition: 0.5s;
  color: white;
}

.icon_class:hover .fa-twitter {
  transition: 0.5s;
  color: white; 
}

.icon_class:hover .fa-linkedin {
  transition: 0.5s;
  color: white; 
}

.icon_class:hover {
  transition: 0.5s ease;
  background-color: #2f3790;
}

.carousel_slide3 {
  display: flex; 
  justify-content: flex-end; 
  align-items: center; 
  position: relative; 
}

/*  #termsdiv > .terms {
    order: 3;
  }.backtotop_btn {
  position: absolute; 
  right: 50px; 
  bottom: 100px;
}

  #termsdiv > .version_number {
    order: 1;
  }*/

.whatwedo_p {
  visibility: visible;
  animation-duration: 1s;
  animation-delay: 0.8s;
  animation-name: fadeInUp; 
  width: 70%; 
  text-align: left; 
  font-size: 13px;
}

.copyright_class {
  display: flex; 
  flex-direction: column; 
  justify-content: center; 
  align-items: center; 
  order: 3;
}

.footer_class {
  display: flex; 
  align-items: center; 
  flex-direction: row-reverse;
  justify-content: space-between;
}

.menu_links.menu_links {
  color: #333!important;
}

.privacy_footer {
  background-color: white; 
  border-top: 0.5px solid lightgrey;
}

.social_icons img:hover {
  cursor: pointer;
}

  .version_number:hover {
    color: #2f3790!important;
  }

#last_submit:hover {
  background-color: grey!important;
  color: #2f3790!important;
}


input[type='number'] {
    -moz-appearance:textfield;
}
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

  #partners_focus {
    width: 80%;
    margin:0 auto;
    text-align: justify;
  }

  .footer_p {
      float: left;
   }

  #partners {
   padding-top: 2.5em;
 }

    .hospitalspan {
   height: 425px;
  }


  #whatwedo {
    padding-top: 40px;
  }

  #referapatients {
    padding-top: 80px;
  }

  #whoweare:focus, #whatwedo:focus, #patients:focus, #referapatients:focus, #partners:focus, #haveQuery:focus {
    outline: none;
  }

  html, body {
    width: 100vw!important;
    overflow-x: hidden;
  }

  .hospital_img {
    margin: 0 auto;
  }

   #meds_equip {
      text-align: left;
   }

.whatweoffer_card {
  display: flex; 
  justify-content: space-around;
  padding: 20px;
}

.what_we_offer_head {
   color: white;
   font-size: 22px;
}

.item {
    height: 100vh;
    background-repeat: no-repeat;
    background-size: cover;
}
.footer-bottom {
   display: flex; 
   justify-content: space-between;
   height: 70px;
   align-items: center;
}

.social_icons {
   width: 100%; 
   margin: 0 auto; 
   display: flex;
   justify-content: center;
}


#slide2_div {
   width: 80%; 
   height: 100vh; 
   margin: 0 auto; 
   display: flex; 
   flex-direction: column;
   justify-content: center!important; 
   align-items: center;
   padding-bottom: 25px;
}

#slide2_div.purpose_class {
   display: flex;
   flex-direction: column;
   justify-content: center;
   align-items: center;
   margin-bottom: 20px;
}

#slide2_div.purpose_class p {
   color: white; 
   margin:0 auto;
   font-size: 14px!important; 
   font-weight: 300;
   margin-top: -10px;
}

#slide2_div.partner_class p {
   color: white; 
   margin:0 auto;
   font-size: 14px!important; 
   font-weight: 300;
}


#slide2_div.serve_class p {
   color: white; 
   margin:0 auto;
   font-size: 14px!important; 
   font-weight: 300;
   margin-top: -10px;
}

#slide2_div.serve_class {
   display: flex;
   flex-direction: column;
   justify-content: center;
   align-items: center;
}
/*
.footer_p {
  position: relative;
  left: 20px;
}*/


.media_icons {
  width: 13em; 
  margin-top: 5px; 
  margin-left: 10px; 
  display: flex; 
  justify-content: space-between; 
  position: absolute; 
  right:45%;
}

#mobile_carousel {
   display: none;
}

/*.serve_class {
   position:relative; 
   top: 40px; 
   left: 330px;
}*/

/*.purpose_class {
   position:relative; 
   right: 300px; 
   top: 150px;
}*/

.wrapperone {
  background-image: linear-gradient(rgba(16, 17, 153, 0.7),rgba(237, 42, 84, 0.7)),url(&quot;images/website_background.jpg&quot;);
  background-repeat: no-repeat;
  background-size: cover;
  data-stellar-background-ratio=0.5;
  display: grid;
  grid-template-rows: 30% 70%; 
  height: auto;
  width: 100vw;
}

.second_cards {
   display: flex;
   flex-direction: column;
   align-items: flex-start;
}

.firstone {
  display: flex;
    flex-direction: column; 
    justify-content: center; 
    align-items: center; 
    width: 80%; 
    margin: 0 auto;
    padding-top: 25px;
}

.secondone {
  display: grid; 
  grid-template-columns: 33% 33% 33%;
}

.secondone_card {
  display: grid; 
  grid-template-columns: 40% 60%; 
  padding: 3em;
  text-align: left;
}

.newfooter {
    display: flex;
    justify-content: flex-start;
}

.partner_img {
    box-shadow: 0 0 0 8px #ddd;
    transition: box-shadow 0.3s;
}

.partner_img:hover{
    box-shadow: 0 0 0 15px #ee2654;
}

.rights_span {
    color: black;
    font-size: 13px;
}

  .privacy_header {
    padding-top: 5%;
  }

@media only screen and (min-width: 700px) {

  .offer_p {
    text-align: left;
  }

.slide-text > p {
  text-align: left;
  font-size: 18px!important;
}

.slide-text > h1 {
  font-size: 30px!important;
}

.slide-text > div > p {
  text-align: left;
  font-size: 18px!important;
}

.slide-text > div > h1 {
  font-size: 30px!important;
}


  .modal-open .modal {
    overflow-y: hidden!important;
  }

  .footerul {
     padding-bottom: 96px;
  }

  .privacy_backtotop {
    left: 83%;
  }

  .tos_backtotop {
    left: 95%;
  }

  .rights_span {
    width: 300px;
    text-align: left;
    position: relative;
    /*right: 66px;*/
  }

  .carousel_slide3 {
    top: 45vh;
  }

  #newlogo {
    margin-top: 10px;
  }


  .footer_class {
    height: 80px;
  }

  #partners {
    padding-bottom: 3.5em;
  }
  #menu_first_link {
    margin-left: 10px;
  }

  #home {
    padding-left: 30px;
    padding-right: 20px;
  }

  .version_number {
    float: right;
  }

  #partners_focus {
    width: 80%;
    margin:0 auto;
    text-align: center;
  }

  #whoweare p {
    width: 68%; 
    margin: 0 auto; 
    text-align: left;
  }

  #termsdiv {
    padding-left: 20px;
    display: flex;
    justify-content: space-around;
  }


  #whatwedo_pricingdiv.fh5co-pricing-style-2 {
    padding: none;
  }

/*  .version_number {
    position: relative;
    right: 13.125em;
  }
*/
    .terms {
      margin-left: -4.155em;
    }

    #know_more {
      margin-bottom: -50px;
      position: relative;
      top: 65px;
    }

   .item {
      background-image: linear-gradient(rgba(16, 17, 153, 0.7),rgba(237, 42, 84, 0.7)),url('images/banner_carousel.jpg');
   }
/*   .social_icons {
      position: relative;
      right: 600px;
   }*/

   #slide2_div {
      margin-top: -100px;
   }

   .purpose_class {
      position: relative;
      right: 280px;
      top: 255px;
      transition: 0.8s;
   }

   .purpose_class p {
      font-size: 22px;
   }

   .serve_class {
      position: relative;
      left: 320px;
      top: 100px;
      transition: 0.8s;
   }

   .serve_class p {
      font-size: 22px;
   }

   .partner_class p {
      font-size: 22px;
   }
}



@media only screen and (max-width: 500px) {

/*  #haveQuery {
    padding-top: 5em;
  }
*/

.closecorner {
  width: 30px;
  height: 30px;
  font-size: 15px;
  padding: 1px 5px 1px 6px;
}


.whatwedo_p {
  padding-bottom: 2em;
}

.slide-text > p {
  text-align: left;
}

.slide-text > div > p {
  text-align: left;
}

  .modal-open::-webkit-scrollbar {
    width: 5px;
  }

  .modal-open::-webkit-scrollbar-track {
  }
 
  .modal-open::-webkit-scrollbar-thumb {
    background-color: red;
    outline: 1px solid blue;
  }


#referpatients_span_hospital {
  bottom: 20px;
}

.referpatients_span {
  position: absolute; 
  bottom: 30px; 
  left: 0; 
  right: 0;
}

#patients {
  background-image: linear-gradient(rgba(16, 17, 153, 0.7),rgba(237, 42, 84, 0.7)),url("images/bg-3.png");
}

.backtotop_btn {
  position: absolute; 
  right: 30px; 
  bottom: 20px;
}

  .referpatients_p {
    font-size: 13px; 
    line-height: 20px; 
    text-align: left; 
    width: 80%; 
    margin: 0 auto;
  }

  .privacy_header {
    padding-top: 25%;
  }

  .privacy_backtotop {
    left: 85%;
  }

  .tos_backtotop {
    left: 85%;
  }


.social_icons {
  padding-bottom: 3em;
}

.footer_class {
  display: flex; 
  align-items: center; 
  flex-direction: column;
  justify-content: space-between;
}

  .navbar-collapse {
    box-shadow: 0px 5px 5px -5px grey;
  }

/*  h2.haveQueryDiv::before { 
  display: block; 
  content: " "; 
  margin-top: 50px; 
  height: 25px; 
  visibility: hidden; 
  pointer-events: none;
}
*/

  .carousel_p {
    width: 280px;
    text-align: center;
    margin: 0 auto;
    font-size: 14px!important;
  }

  .carousel_slide3 {
    top: 40vh;
  }

  .footer_class {
    height: 200px;
  }

  .menu_links {
    color: #777;
  }

  .privacy_footer {
    background-color: #f0f0f8;
  }

  #termsdiv {
    display: flex;
    justify-content: space-around;
    margin-top: 1.5em;
    margin-bottom: 1em;
  }

  .new-logo {
    display: block!important;
  }

  #partners {
   padding-top: 40px;
 }

  .hospitalspan {
   height: 475px;
  }

     .purpose_class {
        margin-top: -50px;
   }

    #whatwedo {
    padding-top: 50px;
  }

    #referapatients {
    padding-top: 85px;
  }

  .hospital_img {
    margin: 10px auto 10px 115px;
  }

  .whatweoffer_card {
    display: flex;
    flex-direction: column; 
    justify-content: space-around;
    align-items: center;
    padding: 20px;
  }

  .whatweoffer_card .second_cards p {
      width: 170%!important;
      text-align: left!important;
      margin-left: 15%;
  }

  .partnerwithus_card {
    padding-bottom: 50px;
  }

  #partners_p {
    width: 80%;
    margin: 0 auto;
    text-align: left!important;
  }

  #partners_focus.black-color-font {
    width: 80%;
    margin: 0 auto;
    text-align: left!important;
  }

  .firstone p {
    width: 80%;
    margin: 0 auto;
    text-align: left!important;
    padding-bottom: 40px;
    word-spacing: 1px;
  }

/*  .referapatientscards {
    margin-bottom: 50px;
  }*/

  .whatwedo_p {
    width: 80%!important;
    margin: 0 auto;
    text-align: left!important;
  }

  #whoweare p {
    width: 73%!important;
    text-align: left!important;
    margin: 0 auto;
  }

  #homepage .new-logo {
    display: none;
  }

  .navbar-brand > img#newlogo {
    position: relative;
    top: 1em;
    background-color: white;
    left: -0.5em;s
  }

  #homepage.site-logo.new-logo {
    display: block!important;
  }

  .oldlogo {
    display: none!important;
  }

  .footer_p {
    position: relative;
    left: 0px;
   }

   .navbar-default .navbar-nav > li > a {
    padding-top: 20px;
   }

   .our_purpose p {
    line-height: 1.5em;
   }

   .impact_class p {
      font-size: 14px;
      line-height: 1.5em;
   }

   .impact_class h1 {
    font-size: 22px;
   }

   .partner_class {
      position: relative;
      top: 130px;
      padding-top: 50px;
   }

   .item {
      width: 350px;
      background-image: linear-gradient(rgba(16, 17, 153, 0.7),rgba(237, 42, 84, 0.7)),url('images/background_small.png');
      background-size: cover;
      background-repeat: no-repeat;
   }

   .partner_class p {
      line-height: 1.5em;
   }

   .what_we_offer_head {
      color: white;
      font-size: 20px;
      padding-bottom: 10px;
      width: 200px;
   }
   .footer-bottom {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
   }
   #slide2_div {
      margin-bottom: -100px;
   }

   .slide-text > div > h1 {
      font-size: 22px;
      margin-bottom: 5px;
   }

   .slide-text > div > know_more {
      padding-top: 10px;
   }
   .slide-text > div > p {
      font-size: 14px;
      margin-bottom: 5px;
      margin-left: 8%;
   }

   #meds_equip {
      text-align: center;
   }
   #slide2_div {
      justify-content: flex-end;
   }
  .pricing-price {
   margin-top: 20px;
  }
  .hospital_pricing {
   margin-left: -10px;
  }
  .hospital_pricing_one {
   position: relative;
   right: 17px;
  }
  .wrapperone {
    width: auto;
  }
  .secondone {
    display: grid;
    grid-template-columns: 100%;
  }
  .secondone_card {
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    margin-bottom: -60px;
  }
  .secondone_card p {
    text-align: left;
  }

  .newfooter {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    height: 100px;
  }
  .media_icons {
    padding-left: 30px;
    margin-right: -70px;
  }

  .second_cards {
   display: flex;
   flex-direction: column;
   align-items: center;
   }
/*   .footerul {
   padding-bottom: 200px;
   }*/
   .footer-bottom p {
      margin-bottom: 10px;
   }
   #slide2_div.purpose_class h1 {
      font-size: 18px;
   }
   #slide2_div.serve_class h1 {
      font-size: 18px;
   }
   .partner_class p {
      font-size: 14px;
   }
   .impact_class.slide_text.slide_style_center {
      position: relative!important;
      bottom: 50px!important;
   }

   .social_icons {
    display: flex;
    justify-content: center;
   }
};

@media only screen and (max-width: 800px) {
  .wrapperone {
    width: auto;
  }
  .secondone {
    display: grid;
    grid-template-columns: 50% 50%;
  }

  .secondone_card {
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    text-align: left;
  }
  .secondone_card p {
    text-align: left;
  }
  .newfooter {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    height: 100px;
  }
};

@media only screen and (max-width: 320px) {
  .wrapperone {
    width: auto;
  }
  .secondone {
    display: grid;
    grid-template-columns: 50% 50%;
  }

  .secondone_card {
    display: grid;
    grid-template-rows: 40% 60%;
    text-align: left;
  }

  .fh5co-cover.fh5co-cover-style-2.js-full-height {
    width: 100vw;
    height: auto;
  }
  .newfooter {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    height: 100px;
  }
  .media_icons {
    padding-left: 30px;
    margin-right: -70px;
  }
   .second_cards {
   display: flex;
   flex-direction: column;
   align-items: center;
   }
}

@media only screen and (min-width: 1000px) {
  .what_we_offer_head {
    text-align: left!important;
  }
}

  #horizontal_form input:-webkit-autofill {
    background-color: white!important;
  }

</style>

</head>
<body>
