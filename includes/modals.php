<!-- 1 -->
<div class="modal fade" id="homecare" role="dialog" tabindex="-1" aria-labelledby="basicModal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form action="" method="POST" id="homecareForm">
            <div class="modal-body">
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <button type="reset" class="btn pull-right closecorner modalclosebtn" onclick="pageReload()" data-dismiss="modal">X</button>
               </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <h4 class="fh5co-heading black-color-font text-center" style="visibility: visible; padding: 30px 0 0"><b>Join our network of Nurses/ Attendants/ Doctors</b></h4>
                  <div class="tab-pane fade in active" id="tab1default3">
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Name</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="text" tabindex="1" class="form-control" name="homecare_name" placeholder="Please Enter Your Name" id="homecare_name" onkeyup="checkNum(6)">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Contact Number </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="number" tabindex="2" oninput="this.value = this.value.slice(0, 10)" min="0" name="homecare_contact" class="form-control" placeholder="e.g.9538439274">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Email </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input tabindex="3" type="text" class="form-control" name="homecare_email" placeholder="Please Enter Your Email">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <label class="popup-check-container">
                              <input type="checkbox" tabindex="3" name="ambulance_" class="modal-checkbox">
                              <span class="popup-checkmark"></span>
                           </label>
                            <label class="col-lg-11 col-md-11 col-sm-12 col-xs-12 control-label font-size-12 no-padding-l neg-margin-top pop-check-label"> I authorize Ribbn to contact me. I understand that this will override the DND status on my mobile number. </label>
                        </div>
                       
                        <div class="clearfix"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="homecareregisterdiv">
                        <button disabled tabindex="4" class="btn btn-primary pull-right save modal-submit-btn" name="homecare_submit" id="homecareregister"> Submit </button>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix"></div>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- 2 -->
<div class="modal fade" id="medical-equipment" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form action="" method="POST" id="medical_form">
            <div class="modal-body">
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <button type="reset" class="btn pull-right closecorner modalclosebtn" onclick="pageReload()" data-dismiss="modal">X</button>
               </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <h4 class="fh5co-heading black-color-font text-center" data-wow-duration="1s" data-wow-delay=".5s" style="visibility: visible;  padding: 30px 0 25px 0; width: 90%; margin: 0 auto"><b>Help us deliver medicines and equipment to the patient</b></h4>
                  <div class="tab-pane fade in active" id="tab1default1">
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label"> Name</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input tabindex="1" type="text" class="form-control" placeholder="Please Enter Your Name" name="medical_name" maxlength="20" required minlength="3" id="medicines_name" onkeyup="checkNum(7)">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label"> Contact Number </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input tabindex="2" type="number" class="form-control" placeholder="e.g.9538439274" name="medical_contact"  maxlength="10" maxlength="10" oninput="this.value = this.value.slice(0, 10)" min="0">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label"> Email </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input tabindex="3" type="text" class="form-control" name="medical_email" placeholder="Please Enter Email ID" required minlength="3">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12control-label"> You are a </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <div class="btn-group width-100 ">
                              <select tabindex="4" type="button" name="medical_select" class="multiselect dropdown-toggle btn multi-btn" data-toggle="dropdown" value="Select Details" style="border-radius: 5px">
                                 <span class="multiselect-selected-text">Select Details</span> <b class="caret"></b>
                                 <option value="Pharmacy">Pharmacy</option>
                                 <option value="Medical Equipment Supplier">Medical Equipment Supplier</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <label class="popup-check-container">
                              <input type="checkbox" tabindex="3" name="ambulance_" class="modal-checkbox">
                              <span class="popup-checkmark"></span>
                           </label>
                            <label class="col-lg-11 col-md-11 col-sm-12 col-xs-12 control-label font-size-12 no-padding-l neg-margin-top pop-check-label"> I authorize Ribbn to contact me. I understand that this will override the DND status on my mobile number. </label>
                        </div>
                       
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="medicalequipmentdiv">
                           <button disabled tabindex="5" class="btn btn-primary pull-right save modal-submit-btn" name="medical_submit" id="medicalequipment"> Submit </button>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- 3 -->
<div class="modal fade" id="refererN" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content text-center">
         <form action="" method="POST" id="myform2">
            <div class="modal-body">
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <button type="reset" class="btn modalclosebtn pull-right closecorner" onclick="pageReload()" data-dismiss="modal">X</button>
               </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <h2 class="fh5co-heading black-color-font" style="visibility: visible; padding: 30px 0 0"> Refer a Patient </h2>
                  <div class="tab-pane fade in active" id="tab1default1">
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label">Referrer's Name</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12" >
                           <input  tabindex="1" type="text" class="form-control" placeholder="Please Enter Referrer's Name" name="referrer_name" onkeyup="checkNum(1)" id="ref_name">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <fieldset class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label">Referrer's Contact Number</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12" >
                           <input tabindex="2" type="number" min="0" oninput="this.value = this.value.slice(0, 10)" class="form-control" placeholder="e.g.9538439274" name="referrer_number">
                        </div>
                        <div class="clearfix"></div>
                     </fieldset>
                     <fieldset class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label">Patient's Name</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12" >
                           <input tabindex="3" type="text" class="form-control" placeholder="Please Enter Patient's Name" name="patient_name" id="ref_pat_name" onkeyup="checkNum(8)">
                        </div>
                        <div class="clearfix"></div>
                     </fieldset>
                     <fieldset class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label">Patient's Contact Number </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input tabindex="4" type="number" oninput="this.value = this.value.slice(0, 10)" min="0" class="form-control" placeholder="e.g.9538439274" name="patient_number">
                        </div>
                        <div class="clearfix"></div>
                     </fieldset>
                     <fieldset class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="submitreferrerdiv">
                           <button tabindex="5" class="btn btn-primary pull-right save" id="submitreferrer" name="submitrefer" type="submit">Submit</button>
                        </div>
                     </fieldset>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- 4 -->
<div class="modal fade" id="ambulance-services" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form action="" method="POST" id="ambulance_form">
            <div class="modal-body">
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <button type="reset" class="btn modalclosebtn pull-right closecorner" onclick="pageReload()" data-dismiss="modal">X</button>
               </div>
               <h3  id="confirmationmessage1"></h3>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="removeelement">
                  <h4 class="fh5co-heading black-color-font text-center" style="visibility: visible; padding: 5px 0 25px 0; width: 90%; margin: 0 auto"><b>Attach your ambulance(s) and driver(s) and grow your business manifold</b></h4>
                  <div class="tab-pane fade in active" id="tab1default1">
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" > Name</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="text" tabindex="1" name="ambulance_name" class="form-control" placeholder="Please Enter Your Name"  id="ambulance_name" onkeyup="checkNum(5)">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label"> Contact Number </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="number" tabindex="2" oninput="this.value = this.value.slice(0, 10)" min="0" name="ambulance_contact" class="form-control" placeholder="e.g.9538439274">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label"> Email </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="text" tabindex="3" name="ambulance_email" class="form-control" placeholder="Please Enter Email ID">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <label class="popup-check-container">
                              <input type="checkbox" tabindex="3" name="ambulance_" class="modal-checkbox">
                              <span class="popup-checkmark"></span>
                           </label>
                           <label class="col-lg-11 col-md-11 col-sm-12 col-xs-12 control-label font-size-12 no-padding-l neg-margin-top pop-check-label"> I authorize Ribbn to contact me. I understand that this will override the DND status on my mobile number. </label>
                        </div>
                        
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                           <button disabled tabindex="4" class="btn btn-primary pull-right save modal-submit-btn" name="ambulance_submit"> Submit </button>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- 5 -->
<div class="modal fade" id="registration_hospital" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content text-center">
         <form action="" method="POST" id="reg_form">
            <div class="modal-body">
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <button type="reset" class="btn modalclosebtn pull-right closecorner" onclick="pageReload()" data-dismiss="modal">X</button>
               </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <h2 class="fh5co-heading black-color-font" style="visibility: visible; padding: 30px 0 0"> Hospital Registration </h2>
                  <div class="tab-pane fade in active" id="tab1default1">
                     <fieldset class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label">Hospital Name</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12" >
                           <input tabindex="1" type="text" class="form-control" placeholder="e.g. Fortis Hospital Group" name="reg_hospital">
                        </div>
                        <div class="clearfix"></div>
                     </fieldset>
                     <fieldset class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label">Primary Contact Name</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12" >
                           <input tabindex="2" type="text" class="form-control" placeholder="Primary Contact's Name" name="reg_name" id="primary_cont_name" onkeyup="checkNum(3)">
                        </div>
                        <div class="clearfix"></div>
                     </fieldset>
                     <fieldset class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label">Primary Contact Number</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12" >
                           <input tabindex="3" type="number" oninput="this.value = this.value.slice(0, 10)" min="0" class="form-control" placeholder="e.g.9538439274" name="reg_contact">
                        </div>
                        <div class="clearfix"></div>
                     </fieldset>
                     <fieldset class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <label class="popup-check-container">
                              <input type="checkbox" tabindex="3" name="ambulance_" class="modal-checkbox">
                              <span class="popup-checkmark"></span>
                           </label>
                           <label class="col-lg-11 col-md-11 col-sm-12 col-xs-12 control-label font-size-12 no-padding-l neg-margin-top pop-check-label"> I authorize Ribbn to contact me. I understand that this will override the DND status on my mobile number. </label>
                        </div>
                        
                        <div class="clearfix"></div>
                     </fieldset>
                     <fieldset class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="hospitalregisterdiv">
                           <button disabled tabindex="4" class="save btn btn-primary pull-right modal-submit-btn" name="reg_submit" id="hospitalregister">Submit</button>
                        </div>
                     </fieldset>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- 6 -->
<div class="modal fade" id="query_pop" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content text-center">
         <div class="modal-body">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
               <h4 id="query_response" class="fh5co-heading black-color-font" style="visibility: visible; padding: 30px 0 0;"></h4>
            </div>
            <div class="clearfix"></div>
            <button type="reset" style="background-color: #2f3790; border: #2f3790" class="btn" onclick="closeQuery()" data-dismiss="modal"><span style="color: white;">Close</span></button>
         </div>
      </div>
   </div>
</div>
<!-- 7 -->
<div class="modal fade" id="requestANurse" role="dialog" tabindex="-1" aria-labelledby="basicModal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form action="" method="POST" id="requestANurseForm">
            <div class="modal-body">
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center">
                  <h4 class="fh5co-heading black-color-font text-center" style="visibility: visible; padding: 15px 0 0;display: inline-block;"><b>Request a Home Nurse</b></h4>
                  <button type="reset" class="btn pull-right closecorner modalclosebtn" onclick="pageReload()" data-dismiss="modal">X</button>
               </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  
                  <div class="tab-pane fade in active" id="tab1default3">
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Name</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="text" tabindex="1" class="form-control" name="nurse_name" placeholder="Please Enter Your Name" id="nurse_name" onkeyup="checkNum(6)">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Contact Number </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="number" tabindex="2" oninput="this.value = this.value.slice(0, 10)" min="0" name="nurse_contact" class="form-control" placeholder="e.g.9538439274">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Email </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input tabindex="3" type="text" class="form-control" name="nurse_email" placeholder="Please Enter Your Email">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <label class="popup-check-container">
                              <input type="checkbox" tabindex="3" name="ambulance_" class="modal-checkbox">
                              <span class="popup-checkmark"></span>
                           </label>
                           <label class="col-lg-11 col-md-11 col-sm-12 col-xs-12 control-label font-size-12 no-padding-l neg-margin-top pop-check-label"> I authorize Ribbn to contact me. I understand that this will override the DND status on my mobile number. </label>
                        </div>
                        
                        <div class="clearfix"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="requestANurseregisterdiv">
                        <button disabled tabindex="4" class="btn btn-primary pull-right save modal-submit-btn" name="requestANurse_submit" id="requestANurseregister"> Submit </button>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix"></div>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- 8 -->
<div class="modal fade" id="requestAnAmbulance" role="dialog" tabindex="-1" aria-labelledby="basicModal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form action="" method="POST" id="requestAnAmbulanceForm">
            <div class="modal-body">
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center">
                  <h4 class="fh5co-heading black-color-font text-center" style="visibility: visible; padding: 15px 0 0;display: inline-block;"><b>Request an Ambulance</b></h4>
                  <button type="reset" class="btn pull-right closecorner modalclosebtn" onclick="pageReload()" data-dismiss="modal">X</button>
               </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  
                  <div class="tab-pane fade in active" id="tab1default3">
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Name</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="text" tabindex="1" class="form-control" name="ambulance_name" placeholder="Please Enter Your Name" id="ambulance_name" onkeyup="checkNum(6)">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Contact Number </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="number" tabindex="2" oninput="this.value = this.value.slice(0, 10)" min="0" name="ambulance_contact" class="form-control" placeholder="e.g.9538439274">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Email </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input tabindex="3" type="text" class="form-control" name="ambulance_email" placeholder="Please Enter Your Email">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <label class="popup-check-container">
                              <input type="checkbox" tabindex="3" name="ambulance_" class="modal-checkbox">
                              <span class="popup-checkmark"></span>
                           </label>
                           <label class="col-lg-11 col-md-11 col-sm-12 col-xs-12 control-label font-size-12 no-padding-l neg-margin-top pop-check-label"> I authorize Ribbn to contact me. I understand that this will override the DND status on my mobile number. </label>
                        </div>
                        
                        <div class="clearfix"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="requestAnAmbulanceregisterdiv">
                        <button disabled tabindex="4" class="btn btn-primary pull-right save modal-submit-btn" name="requestAnAmbulance_submit" id="requestAnAmbulanceregister"> Submit </button>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix"></div>
            </div>
         </form>
      </div>
   </div>
</div>


<!-- 9 -->
<div class="modal fade" id="requestMedicalEquipments" role="dialog" tabindex="-1" aria-labelledby="basicModal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form action="" method="POST" id="requestMedicalEquipmentsForm">
            <div class="modal-body">
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center">
                  <h4 class="fh5co-heading black-color-font text-center" style="visibility: visible; padding: 15px 0 0;display: inline-block;"><b>Request Med Equipment</b></h4>
                  <button type="reset" class="btn pull-right closecorner modalclosebtn" onclick="pageReload()" data-dismiss="modal">X</button>
               </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  
                  <div class="tab-pane fade in active" id="tab1default3">
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Name</label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="text" tabindex="1" class="form-control" name="med_eqp_name" placeholder="Please Enter Your Name" id="med_eqp_name" onkeyup="checkNum(6)">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Contact Number </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input type="number" tabindex="2" oninput="this.value = this.value.slice(0, 10)" min="0" name="med_eqp_contact" class="form-control" placeholder="e.g.9538439274">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-5 col-lg-5 col-sm-12 col-xs-12 control-label" style="color:#808080"> Email </label>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                           <input tabindex="3" type="text" class="form-control" name="med_eqp_email" placeholder="Please Enter Your Email">
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="form-group">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <label class="popup-check-container">
                              <input type="checkbox" tabindex="3" name="ambulance_" class="modal-checkbox">
                              <span class="popup-checkmark"></span>
                           </label>
                           <label class="col-lg-11 col-md-11 col-sm-12 col-xs-12 control-label font-size-12 no-padding-l neg-margin-top pop-check-label"> I authorize Ribbn to contact me. I understand that this will override the DND status on my mobile number. </label>
                        </div>
                        
                        <div class="clearfix"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="med_eqp_registerdiv">
                        <button disabled tabindex="4" class="btn btn-primary pull-right save modal-submit-btn" name="med_eqp_submit" id="med_eqp_register"> Submit </button>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix"></div>
            </div>
         </form>
      </div>
   </div>
</div>


<!-- <script>
    $(document).ready(function() {
        //1
        $("#myform2").submit(function(event) {
            event.preventDefault();
            if ($('#myform2').valid()) {
                var $form = $(this),
                    name = $form.find("input[name='referrer_name']").val(),
                    num = $form.find("input[name='referrer_number']").val(),
                    nameTwo = $form.find("input[name='patient_name']").val(),
                    numTwo = $form.find("input[name='patient_number']").val(),
                    url = $form.attr("action");
                var posting = $.post("includes/caregiver_refer.php", {
                    referrer_name: name,
                    referrer_number: num,
                    patient_name: nameTwo,
                    patient_number: numTwo
                }).done(function(data) {
                    if (data == 'success') {
                        $("#query_response").text("Thank you for your valuable referral. We will get in touch with the patient soon.");
                        $form.find("input[name='referrer_name']").val('');
                        $form.find("input[name='referrer_number']").val('');
                        $form.find("input[name='patient_name']").val('');
                        $form.find("input[name='patient_number']").val('');
                        $('#refererN').modal('hide');
                        setTimeout(function() { $('#query_pop').show(); }, 300);
                        $(document).on('click', function() {
                            $("#query_pop").hide();
                        })

                    } else {

                    }
                });
            }

        });
        //2
        $("#homecareForm").submit(function(event) {
            event.preventDefault();
            if ($('#homecareForm').valid()) {
                var $form = $(this),
                    name = $form.find("input[name='homecare_name']").val(),
                    contact = $form.find("input[name='homecare_contact']").val(),
                    email = $form.find("input[name='homecare_email']").val(),
                    url = $form.attr("action");
                var posting = $.post("includes/homecare.php", {
                    homecare_name: name,
                    homecare_contact: contact,
                    homecare_email: email
                }).done(function(data) {
                    if (data == 'success') {
                        $("#query_response").text("Thank you for your interest in Ribbn. We will get in touch with you soon.");
                        $form.find("input[name='homecare_name']").val('');
                        $form.find("input[name='homecare_contact']").val('');
                        $form.find("input[name='homecare_email']").val('');
                        $('#homecare').modal("hide");
                        setTimeout(function() { $('#query_pop').show(); }, 300);
                        $(document).on('click', function() {
                            $("#query_pop").hide();
                        })

                    } else {

                    }
                });
            }

        });


         $("#requestANurseForm").submit(function(event) {
            event.preventDefault();
            if ($('#requestANurseForm').valid()) {
               var $form = $(this),
                  name = $form.find("input[name='nurse_name']").val(),
                  contact = $form.find("input[name='nurse_contact']").val(),
                  email = $form.find("input[name='nurse_email']").val(),
                  url = $form.attr("action");
                  var posting = $.post("includes/requestService.php", {
                     service_type : 'Nurse',
                     contact_name: name,
                     contact_number: contact,
                     contact_email: email
                  }).done(function(data) {
                     if (data == 'success') {
                        $("#query_response").text("Thank you for your interest in Ribbn. We will get in touch with you soon.");
                        $form.find("input[name='nurse_name']").val('');
                        $form.find("input[name='nurse_contact']").val('');
                        $form.find("input[name='nurse_email']").val('');
                        $('#requestANurse').modal("hide");
                        setTimeout(function() { $('#query_pop').show(); }, 300);
                        $(document).on('click', function() {
                            $("#query_pop").hide();
                        })
                     } else {
                     }
                  });
            }
         });


        //3
        $("#medical_form").submit(function(event) {
            event.preventDefault();
            if ($('#medical_form').valid()) {
                var $form = $(this),
                    name = $form.find("input[name='medical_name']").val(),
                    contact = $form.find("input[name='medical_contact']").val(),
                    email = $form.find("input[name='medical_email']").val(),
                    select = $form.find("select[name='medical_select']").val(),
                    url = $form.attr("action");
                var posting = $.post("includes/medical.php", {
                    medical_name: name,
                    medical_contact: contact,
                    medical_email: email,
                    medical_select: select
                }).done(function(data) {
                    if (data == 'success') {
                        $("#query_response").text("Thank you for your interest in Ribbn. We will get in touch with you soon.");
                        $form.find("input[name='medical_name']").val('');
                        $form.find("input[name='medical_contact']").val('');
                        $form.find("input[name='medical_email']").val('');
                        $form.find("input[name='medical_select']").val('');
                        $('#medical-equipment').modal("hide");
                        setTimeout(function() { $('#query_pop').show(); }, 300);
                        $(document).on('click', function() {
                            $("#query_pop").hide();
                        })

                    } else {

                    }
                });
            }

        });
        //4
        $("#ambulance_form").submit(function(event) {
            event.preventDefault();
            if ($('#ambulance_form').valid()) {
                var $form = $(this),
                    name = $form.find("input[name='ambulance_name']").val(),
                    contact = $form.find("input[name='ambulance_contact']").val(),
                    email = $form.find("input[name='ambulance_email']").val(),
                    url = $form.attr("action");
                var posting = $.post("includes/ambulance.php", {
                    ambulance_name: name,
                    ambulance_contact: contact,
                    ambulance_email: email
                }).done(function(data) {
                    if (data == 'success') {
                        $("#query_response").text("Thank you for your interest in Ribbn. We will get in touch with you soon.");
                        $form.find("input[name='ambulance_name']").val('');
                        $form.find("input[name='ambulance_contact']").val('');
                        $form.find("input[name='ambulance_email']").val('');
                        $('#ambulance-services').modal('hide');
                        setTimeout(function() { $('#query_pop').show(); }, 300);
                        $(document).on('click', function() {
                            $("#query_pop").hide();
                        })
                    } else {

                    }
                });
            }
        });
        //5
        $("#reg_form").submit(function(event) {
            event.preventDefault();
            if ($('#reg_form').valid()) {
                var $form = $(this),
                    hosp = $form.find("input[name='reg_hospital']").val(),
                    name = $form.find("input[name='reg_name']").val(),
                    contact = $form.find("input[name='reg_contact']").val(),
                    url = $form.attr("action");
                var posting = $.post("includes/registration.php", {
                    reg_hospital: hosp,
                    reg_name: name,
                    reg_contact: contact
                }).done(function(data) {
                    if (data == 'success') {
                        $("#query_response").text("Thank you. Our team will get in touch with you soon.");
                        $form.find("input[name='reg_hospital']").val('');
                        $form.find("input[name='reg_name']").val('');
                        $form.find("input[name='reg_contact']").val('');
                        $('#registration_hospital').modal('hide');
                        setTimeout(function() { $('#query_pop').show(); }, 300);
                        $(document).on('click', function() {
                            $("#query_pop").hide();
                        })
                    } else {}
                });
            }
        });
        //6
        $("#query_form").submit(function(event) {
            event.preventDefault();
            if ($('#query_form').valid()) {
                var $form = $(this),
                    name = $form.find("textarea[name='querymessage']").val(),
                    email = $form.find("input[name='queryemail']").val(),
                    url = $form.attr("action");
                var posting = $.post("includes/query.php", {
                    querymessage: name,
                    queryemail: email
                }).done(function(data) {
                    if (data == 'success') {
                        $("#query_response").text("Thank you for writing to us. We will get in touch with you soon.");
                        $form.find("textarea[name='querymessage']").val('');
                        $form.find("input[name='queryemail']").val('');
                        setTimeout(function() { $('#query_pop').show(); }, 300);
                        $(document).on('click', function() {
                            $("#query_pop").hide();
                        })
                    } else {

                    }
                });
            }
        });
    });


    function pageReload() {
        $("#server-response").css('display', 'none');
    }

    function closeQuery() {
        $('#query_pop').hide();
    }

    $(document).ready(function() {
        $('#homecare').on("shown.bs.modal", function() {
            $(this).find(".form-control:first").focus();
        });
        $('#medical-equipment').on("shown.bs.modal", function() {
            $(this).find(".form-control:first").focus();
        });
        $('#refererN').on("shown.bs.modal", function() {
            $(this).find(".form-control:first").focus();
        });
        $('#ambulance-services').on("shown.bs.modal", function() {
            $(this).find(".form-control:first").focus();
        });
        $('#registration_hospital').on("shown.bs.modal", function() {
            $(this).find(".form-control:first").focus();
        });
        $(document).on('click', '.contactbtn', function() {
            $('html, body').animate({ scrollTop: $('#haveQuery').offset().top }, 'slow');
            $('#query_form').find(".form-control:first").focus();
        });
    });

    function checkNum(val) {
        //ref_pat_name
        if (val == 1) {
            var number = document.getElementById('ref_name');
        } else if (val == 2) {
            var number = document.getElementById('ref_pat_name');
        } else if (val == 3) {
            var number = document.getElementById('primary_cont_name');
        } else if (val == 4) {
            var number = document.getElementById('primary_cont_name');
        } else if (val == 5) {
            var number = document.getElementById('ambulance_name');
        } else if (val == 6) {
            var number = document.getElementById('homecare_name');
        } else if (val == 7) {
            var number = document.getElementById('medicines_name');
        } else if (val == 8) {
            var number = document.getElementById('ref_pat_name');
        }
        number.onkeydown = function(e) {
            if (!((e.keyCode > 64 && e.keyCode < 91) || (e.keyCode > 96 && e.keyCode < 123) || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 32 || e.keyCode == 37  || e.keyCode == 39 || e.keyCode == 46)) {
                return false;
            }
        }

    }
</script> -->