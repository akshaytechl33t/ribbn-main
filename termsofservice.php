<?php $pageTitle = 'Terms of Service'; include("includes/header.php"); ?>
<nav class="navbar navbar-default navbar-me" style="background-color: white">
   <div class="container-fluid ">
      <div class="navbar-header">
        <button id="trigger" type="button" class="navbar-toggle collapsed menu-collapsed-button" data-toggle="collapse" data-target="#navbar-primary-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
         <a class="navbar-brand site-logo" href="index.php" id="home" ><img id="newlogo" data-target="index.php" src="images/ribbn_newlogo2x.png" width="150" height="50" alt="" style="padding-top: 0.2em; padding-bottom: 0.2em" /></a> 
      </div>
      <div class="collapse navbar-collapse " id="navbar-primary-collapse" style="box-shadow: 0px 5px 5px -5px grey">
         <ul class="nav navbar-nav">
            <li> <a id="menu_first_link" class="menu_links" href="index.php#whoweare" style="font-weight: 600;">WHO WE ARE</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="menu_links" href="index.php#whatwedo" style="font-weight: 600;">WHAT WE DO</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="menu_links" href="index.php#patients" style="font-weight: 600;">WHAT WE OFFER</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="menu_links" href="index.php#referapatients" style="font-weight: 600;">REFER A PATIENT</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="menu_links" href="index.php#partners" style="font-weight: 600;">PARTNER WITH US</a> </li>
         </ul>
         <ul class="nav navbar-nav navbar-right">
            <li>
               <a href="index.php#haveQuery" class="menu_links" data-target="index.php#haveQuery" style="font-weight: 600; padding-top: 18px; padding-bottom: 1.5rem">Contact Us</a>
            </li>
            <li class="margin-Top-sr"></li>
         </ul>
      </div>
   </div>
</nav>
<div class="container" style="background-color: white; color: black; margin-top: 70px; text-align: left;" id="tos_cont">
<h3 class="text-center privacy_header" style="color: #2f3790; text-decoration: underline;">Terms Of Service</h3>
<p style="color: black; text-align: left"><b>Acceptance of the Terms of Use</b><br><br>
   Welcome to <a href="index.php" style="color: #2f3790">ribbn.in</a>, the website of Ribbn Healthcare Management Services, a company dedicated to providing healthcare management services, (hereinafter referred to in this document as "Ribbn.in", “ribbn” or "us", "we" or "our"). The following terms and conditions, (“Terms of Use”), apply to your use of the <a href="index.php" style="color: #2f3790">www.ribbn.in</a> website, and the content. Please read these Terms of Use carefully before using the website. By using the website, you accept and agree to abide by these Terms of Use. Do not use this website if you do not agree to these Terms of Use.<br><br>
   <b>Amendments and Notice</b><br><br>
   We may, from time to time, at our sole discretion and without advance notice, amend these Terms of Use. Please visit this page regularly to make yourself aware of the changes as they are binding on you.<br><br>
   <b>Location</b><br><br>
   The website is intended for use in India.<br><br>
   <b>Disclaimer</b><br><br>
   Ribbn Care Management Services is not providing medical advice or medical service(s) in any manner whatsoever. All of the content on this website is intended for information purposes only. It is not be used for medical diagnosis, treatment or as a substitute for professional services of qualified medical and / or health professionals. Always seek the advice of your Doctor and / or your professional medical or health adviser before making any changes to your healthcare plans.<br><br>
   <b>Ownership and Intellectual Property Rights</b><br><br>
   The entire contents of the website (including, text, images, pictures, videos, audio files, functionality and the URL, <a href="index.php" style="color: #2f3790">www.ribbn.in</a>,  (collectively the “Content”), are the property of Ribbn Healthcare Management Services, and are protected by International copyright, patents, copyright, trademarks, and other intellectual property (IP) laws. Your use of this website and the information herein is permitted for your personal, noncommercial use only; if you are associated with Us as a customer, provider or partner the use of the website and its contents herein is permitted for legitimate business purposes and as laid out in the terms of the agreement(s). <br><br>
   Your use of this website and its content herein does not grant you the right to reproduce, sell or exploit for any commercial purposes any part of the website and the content within, or use the website or any of its content without obtaining express permission or license to do so from Us. <br><br>
   Any act of or attempt to breach the above terms, including, but not limited to, printing, copying, or otherwise use any part of the website, will result in immediate termination of your right to use the website. No right, title or interest in or to the website and the content within are transferred to you, and any rights not expressly granted are reserved by Us. Any use of the website not expressly permitted by these Terms of Use is a breach of these Terms of Use and may violate copyright, trademark and other laws.<br><br>
   <b>Accessing the Website</b><br><br>
   To use this website, you must be of a legal age as defined in the law of the state and in any case not less than 18 (eighteen) years. If you do not meet this requirement, kindly exit this website immediately. Access to the website is permitted on a temporary basis, and we reserve the right to withdraw or amend any service we provide on the website at our sole discretion without notice. We will not be liable if all or any part of the website is unavailable at any time or for any period. <br><br>
   We collect and use information, including but not limited to, details of the device and the IP address from where you access the website, in accordance with our <a href="privacy.php" style="color:#2f3790">Privacy Policy</a>. By accessing the website, you consent to such collection and use.<br><br>
   Access to the website or some sections within may require you to register once and thereafter use the credentials provided to you, to login to the website, especially sections to which access is granted. It is your responsibility to ensure that all the information you provide is, correct, complete and up to date. Additionally it is your responsibility to safeguard the credentials provided to you to login to the services. By providing the information you agree to receive information from time to time from Ribbn. Notwithstanding any of the above, if we believe that an user is not compliant to the Terms of Use, Ribbn.in reserves the right to disable and terminate access to the registered user, without cause or notice. Please review the <a href="privacy.php" style="color:#2f3790">Privacy Policy</a> for more details.<br><br>
   <b>User Information</b><br><br>
   The website contains certain sections where user information, including but not limited to personal details viz., name, age, date of birth, address, and other contact details, health records, discharge summary, employment details, banking information, insurance coverage details, business details viz., Entity Name & registration details, Financials, Bank Account Details, (collectively referred to as “User Information”), is collected. You represent and warrant that such information is accurate and up to date and you must take responsibility to update this to reflect any changes. By providing this information you grant Ribbn.in permission to use and share the same with our partners and service providers.  <br> <br>
   <b>Acceptable Use Policy</b><br><br>
   Access to and use of this website is only for lawful purposes and in accordance with these Terms of Use. <br><br>
   <b>Links to Third-Party Sites</b><br><br>
   The website may contain links to, third-party websites not under the control of Ribbn.in. Ribbn.in is not responsible for the content and availability of linked third-party sites. Ribbn.in encourages you to undertake necessary and appropriate due diligence before proceeding with accessing these third party linked sites.<br><br>
   <b>Disclaimer of Warranties With Respect to Website</b><br><br>
   Ribbn.in does not represent or guarantee the accuracy or reliability of any of the Content, nor the quality of any information or other materials or services displayed, sold, or purchased by you as a result of your use of our website. You acknowledge that any reliance upon the information or materials shall be at your sole risk. Unless otherwise provided in these Terms of Use, Ribbn.in reserves the right, at its discretion, to correct any error or omissions in any portion of our website, with or without notice to you.<br><br>
   NOTWITHSTANDING ANYTHING CONTAINED HEREIN TO THE CONTRARY, THE WEBSITE AND ALL INFORMATION HEREIN IS PROVIDED "AS IS" AND "AS AVAILABLE," WITHOUT ANY WARRANTIES OR CONDITIONS WHATSOEVER, EXPRESSED OR IMPLIED. NOTWITHSTANDING THE FOREGOING OR ANY STATEMENT TO THE CONTRARY CONTAINED IN THESE TERMS OF USE, Ribbn.in DOES NOT WARRANT THAT THE CONTENT WILL MEET YOUR REQUIREMENTS. Ribbn.in HEREBY DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES OF ANY KIND, WHETHER EXPRESS, STATUTORY, OR IMPLIED, ORAL OR WRITTEN, WITH RESPECT TO THE WEBSITE AND ITS CONTENT, INCLUDING WITHOUT LIMITATION ALL IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON‐INFRINGEMENT, ACCURACY, RELIABILITY, COMPLETENESS, OR TIMELINESS, AND ALL WARRANTIES IMPLIED FROM ANY COURSE OF DEALING, COURSE OF PERFORMANCE, OR USAGE OF TRADE.<br><br>
   <b>Limitations of Liability</b><br><br>
   IN NO EVENT WILL Ribbn.in OR ITS AFFILIATES OR ANY OF THEIR RESPECTIVE EMPLOYEES, OFFICERS, DIRECTORS, SHAREHOLDERS, AGENTS, REPRESENTATIVES, LICENSORS, OR SUPPLIERS BE LIABLE TO YOU OR TO ANY THIRD PARTY FOR ANY DIRECT, SPECIAL, CONSEQUENTIAL, INDIRECT, OR INCIDENTAL LOSSES, DAMAGES, OR EXPENSES, DIRECTLY OR INDIRECTLY RELATING TO THE USE OR MISUSE OF OUR WEBSITE, OR WITH RESPECT TO ANY OTHER HYPERLINKED WEBSITE, OR THE CONTENT USED HEREIN OR THEREWITH, WHETHER IN AN ACTION IN CONTRACT, TORT, STRICT LIABILITY, NEGLIGENCE, OR OTHERWISE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. <br><br>
   <b>Indemnification</b><br><br>
   You agree to indemnify and hold harmless Ribbn.in, its subsidiaries, affiliates, directors, officers, agents, vendors or other partners, and employees, from any claim or demand, including attorneys' fees, due to or arising out of (i) your breach of this Terms of Use, or your violation of any law or the rights of a third party; or (ii) any material or information posted, provided, transmitted, or otherwise made available by you on the website.<br><br>
   <b>Governing Law</b><br><br>
   These Terms of Use shall be governed by the laws of India where these services are intended to be provided. <br><br>
   This Terms of Use, and any additional terms referenced herein, constitute the entire Terms of Use between Ribbn.in and you with respect to the website. If any provision of this Terms of Use is found to be invalid or unenforceable, the remaining provisions shall be enforced to the fullest extent possible, and the remaining Terms of Use shall remain in full force and effect. This Terms of Use inures to the benefit of Ribbn.in, its successors and assigns.<br><br>
   Please use the <a href="index.php#haveQuery" style="color:#2f3790" class="contactbtn">Contact Us</a> page for any comments or questions regarding the website.<br><br>
</p>
<!-- <a class="privacy_backtotop" data-target="#fh5co-page" alt="Back To Top" href="#tos_cont" style="position: relative; bottom: 20px"><img style="width:35px; height: 35px" src="images/top_arrow.svg"></a> -->
<a class="tos_backtotop" alt="Back To Top" href="#tos_cont"><img style="width:50px; height: 50px" src="images/top_arrow.svg"></a>

</div>

<footer class="privacy_footer">
<!--   <div class="container-fluid">
    <div class="row" style="height: 60px">
       <div class="col-md-4 col-sm-12 col-xs-12 text-center" style="display: flex; justify-content: center;">
          <p style="color: black; text-align: justify;"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.<br><span class="rights">All Rights Reserved.</span></p>
       </div>
       <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="social_icons">
             <a href="https://www.facebook.com/Ribbnhealth/"><img src="images/facebook-logo.svg" style="width: 30px; height: 30px; margin-left: 10px"></a>
             <a href="https://www.linkedin.com/company/ribbn/"><img src="images/linkedin.svg" style="width: 30px; height: 30px; margin-left: 20px"></a>
             <a href="https://twitter.com/RibbnHealth"><img src="images/twitter-social.svg" style="width: 30px; height: 30px; margin-left: 20px"></a>
          </div>
       </div>
       <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv">
          <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">Privacy Policy</a>
          <a href="termsofservice.php" style="color: #2f3790; font-size: 14px; margin-right:5px" class="terms">Terms of Service</a>
          <span style="font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>
       </div>
    </div>
  </div> -->

              <div class="row footer_class">
               <div class="col-md-4 col-sm-12 col-xs-12 text-center copyright_class">
                  <p class="copyright_p"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="social_icons">
                     <a target="_blank" rel="noopener" href="https://www.facebook.com/Ribbnhealth/" class="icon_class"><i class="fa fa-facebook"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://www.linkedin.com/company/ribbn/" class="icon_class"><i class="fa fa-linkedin"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://twitter.com/RibbnHealth" class="icon_class"><i class="fa fa-twitter"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="blog-headings.php" class="icon_class">
                        <i class="fa fa-rss-square" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv" style="order: -1">
                  <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">PRIVACY POLICY</a>
                  <a href="termsofservice.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:-1em" class="terms">TERMS OF SERVICE</a>
               </div>
               <!-- <span style="order:5; font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>-->
            </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

<script type="text/javascript">
        $(document).ready(function() {
        $('#trigger').click(function(event) {
            event.stopPropagation();
            $('#navbar-primary-collapse').toggle();

            $(document).click(function() {
                $('#navbar-primary-collapse').hide();
            });

        });
    });
</script>

<script type="text/javascript">
  $(document).ready(function() {
      function parse_query_string(query) {
        var vars = query.split("&");
        var query_string = {};
        for (var i = 0; i < vars.length; i++) {
          var pair = vars[i].split("=");
          // If first entry with this name
          if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
          } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
          } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
          }
        }
        return query_string;
      };
  });
</script>




</body>
</html>
