<?php $pageTitle = 'Home Care'; include("includes/header.php"); ?>
<nav class="navbar navbar-default navbar-me">
    <div class="container-fluid ">
       <div class="navbar-header">
          <button id="trigger" type="button" class="navbar-toggle collapsed menu-collapsed-button press-to-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand site-logo oldlogo" href="index.php" id="home"><img src="images/logo.png" data-target="#fh5co-page" class="" width="103" height="165" alt=""/></a> <a class="navbar-brand site-logo new-logo" href="index.php" id="home" ><img id="newlogo" data-target="#fh5co-page" src="images/ribbn_newlogo2x.png" width="150" height="50" alt="" style="padding-top: 0.2em; padding-bottom: 0.2em" /></a> 
       </div>
       <div class="collapse navbar-collapse " id="navbar-primary-collapse">
          <ul class="nav navbar-nav">
             <li> <a class="" href="index.php#whoweare" style="font-weight: 600;">WHO WE ARE</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#whatwedo" style="font-weight: 600;">WHAT WE DO</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#patients" style="font-weight: 600;">WHAT WE OFFER</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#referapatients" style="font-weight: 600;">REFER A PATIENT</a> </li>
             <li class="margin-Top10">.</li>
             <li> <a class="" href="index.php#partners" style="font-weight: 600;">PARTNER WITH US</a> </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li>
                 <a href="index.php#footer" style="font-weight: 600; padding-top: 18px; padding-bottom: 1.5rem" class="contactbtn">Contact Us</a>
              </li>
              <li class="margin-Top-sr"></li>
           </ul>
       </div>
    </div>
 </nav>
<div class="container-fluid med-page-banner nurse" style="overflow-x: hidden;">
    <div class="med-banner-icon nurse">
        <img src="images/blog-icon.svg">
    </div>
    <h2> Ribbn Blog </h2>
</div>
<div class="container-fluid clearfix mob-container shrunk-container" style="background-color: white; color: black;text-align: left; padding-top: 3%;overflow-x: hidden;" id="tos_cont">
    <!-- home care -->
    <div class="col-md-9 col-sm-12 no-padding-left homecareshow homecare no-xs-padding-r">
        <h3 class="blog-list-head"> Relevant Articles </h3>
        <div class="blog-wrapper clearfix">
            <span>
              1.
            </span>
            <span>
              <h2>  
                <a href="finding-doctor.php"> 
                  Finding the RIGHT doctor - <span class="font-style-normal dark-grey-text"> by VN Bhattacharya </span>
                </a>
              </h2>
            </span>
        </div>

        <div class="blog-wrapper clearfix">
            <span>
              2.
            </span>
            <span>
               <h2>  
                <a href="holistic-nursing.php"> 
                  Holistic Nursing - <span class="font-style-normal dark-grey-text"> by Shelly Ferman Autor </span>
                </a>
              </h2>
            </span>
        </div>

         <div class="blog-wrapper clearfix">
            <span>
              3.
            </span>
            <span>
               <h2>  
                <a href="frailty-and-resilience.php"> 
                  On frailty and resilience - <span class="font-style-normal dark-grey-text"> by Srikanth Rajagopalan </span>
                </a>
              </h2>
            </span>
        </div>

    </div>
    <div class="col-md-3 col-sm-12 homecareshow homecare">

        <!-- <div class="col-sm-3 col-sm-offset-1 no-padding-right no-padding-left"> -->
          <button class="thum-btn" type="button" data-toggle="modal" data-target="#requestANurse"> 
            <img src="images/Pointing-Finger.svg" class="pointingFingerClass"> <span class="pointingFingerSpanClass margin-left-min-10" > Request a Home Nurse </span>
            <!-- style="margin-right: auto; margin-left: -30px;" -->
          </button>
        <!-- </div> -->
        
        <a href="home-care-palliative.php">
          <div class="each-article-thumb homecaresection" data-navaigate-to="palliativecare">
              <div class="second-overlay"> </div>
              <img src="images/smallthumb-three.jpg">
              <div class="art-thumb-overlay">
                  <span class="top-title"> 
                  DID YOU KNOW
                  </span>
                  <p> 
                     Hospice care is only for patients who are no longer receiving curative treatments for their illness, and want to focus only on quality of life.
                  </p>
              </div>
          </div>
        </a>
        <a href="medical-equipments.php">
          <div class="each-article-thumb read-also">
              <div class="second-overlay"> </div>
              <img src="images/smallthumb-one.jpg">
              <div class="art-thumb-overlay nurse-read-more">
                  <span class="top-title"> 
                  ALSO READ
                  </span>
                  <p> 
                     Continued hospital stay post-surgery/ stabilization proves expensive for the hospital as well as the patient.
                  </p>
              </div>
          </div>
        </a>
    </div>
    <p class="med-article-title">
      <span>  
      OTHER SERVICES
      </span>
  </p>
  <div class="bottom-icon-wrapper">
    <a href="concierge.php">
      <div class="each-buttom-icon">
          <img class="reverse-image" src="images/AMBULANCE.svg">
          <p> AMBULANCE </p>
      </div>
    </a>
    <a href="medical-equipments.php">
      <div class="each-buttom-icon">
          <img src="images/medicines.svg">
          <p> MED EQUIPMENT </p>
      </div>
    </a>
    
  </div> 
</div>


<?php  include("includes/modals.php"); ?>
<footer class="privacy_footer">
<!--   <div class="container-fluid">
    <div class="row" style="height: 60px">
       <div class="col-md-4 col-sm-12 col-xs-12 text-center" style="display: flex; justify-content: center;">
          <p style="color: black; text-align: justify;"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.<br><span class="rights">All Rights Reserved.</span></p>
       </div>
       <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="social_icons">
             <a href="https://www.facebook.com/Ribbnhealth/"><img src="images/facebook-logo.svg" style="width: 30px; height: 30px; margin-left: 10px"></a>
             <a href="https://www.linkedin.com/company/ribbn/"><img src="images/linkedin.svg" style="width: 30px; height: 30px; margin-left: 20px"></a>
             <a href="https://twitter.com/RibbnHealth"><img src="images/twitter-social.svg" style="width: 30px; height: 30px; margin-left: 20px"></a>
          </div>
       </div>
       <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv">
          <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">Privacy Policy</a>
          <a href="termsofservice.php" style="color: #2f3790; font-size: 14px; margin-right:5px" class="terms">Terms of Service</a>
          <span style="font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>
       </div>
    </div>
  </div> -->


              <div class="row footer_class">
               <div class="col-md-4 col-sm-12 col-xs-12 text-center copyright_class">
                  <p class="copyright_p for-desk-top"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>

                  <p class="copyright_p for-mobile"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="social_icons">
                     <a target="_blank" rel="noopener" href="https://www.facebook.com/Ribbnhealth/" class="icon_class"><i class="fa fa-facebook"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://www.linkedin.com/company/ribbn/" class="icon_class"><i class="fa fa-linkedin"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://twitter.com/RibbnHealth" class="icon_class"><i class="fa fa-twitter"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="blog-headings.php" class="icon_class">
                        <i class="fa fa-rss-square" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv" style="order: -1">
                  <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">PRIVACY POLICY</a>
                  <a href="termsofservice.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:-1em" class="terms">TERMS OF SERVICE</a>
               </div>
               <!-- <span style="order:5; font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.01</span>-->
            </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
 -->

<script type="text/javascript" src="css/bootstrap.min.js"></script>

<script type="text/javascript">
        $(document).ready(function() {
        $('#trigger').click(function(event) {
            event.stopPropagation();
            $('#navbar-primary-collapse').toggle();

            $(document).click(function() {
                $('#navbar-primary-collapse').hide();
            });

        });
    });
</script>

<script type="text/javascript">
  $(document).ready(function() {
      function parse_query_string(query) {
        var vars = query.split("&");
        var query_string = {};
        for (var i = 0; i < vars.length; i++) {
          var pair = vars[i].split("=");
          // If first entry with this name
          if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
          } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
          } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
          }
        }
        return query_string;
      };

      // $(".homecareshow.homecare").show();
      // $(".homecareshow.palliativecare").hide();
  });

  $(window).scroll(function() {
        if ($(window).width() > 500) {
            if ($(this).scrollTop() > 5) {
                $(".navbar-me").addClass("fixed-me");
                $('nav').addClass('shrink');
                $('.oldlogo').hide();
                $('.new-logo').show();
            } else {
                $(".navbar-me").removeClass("fixed-me");
                $('nav').removeClass('shrink');
                $('.oldlogo').show();
                $('.new-logo').hide();
            }
        }
    });
</script>

<script>

       $('.modal-checkbox').click(function() {
        if ($(this).is(':checked')) {
           $(this).parents('form').find('.modal-submit-btn').removeAttr('disabled');
        } else {
           $(this).parents('form').find('.modal-submit-btn').attr('disabled', 'disabled');
        }
    });

       $('#trigger').click(function(){

          $(this).parent().next('.navbar-collapse').toggleClass('display-blck');

      });
</script>
<?php include("includes/footer.php"); ?>
</body>
</html>
