<?php $pageTitle = 'Welcome to Ribbn';   include("includes/header.php");  ?>
<div id="fh5co-page">
<nav class="navbar navbar-default navbar-me">
   <div class="container-fluid ">
      <div class="navbar-header">
         <button id="trigger" type="button" class="navbar-toggle collapsed menu-collapsed-button" data-toggle="collapse" data-target="#navbar-primary-collapse" aria-expanded="false"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
         </button>
         <a class="navbar-brand site-logo oldlogo" href="index.php" id="home"><img src="images/logo.png" data-target="#fh5co-page" class="" width="103" height="165" alt=""/></a> <a class="navbar-brand site-logo new-logo" href="index.php" id="home" ><img id="newlogo" data-target="#fh5co-page" src="images/ribbn_newlogo2x.png" width="150" height="50" alt="" style="padding-top: 0.2em; padding-bottom: 0.2em" /></a> 
      </div>
      <div class="collapse navbar-collapse " id="navbar-primary-collapse">
         <ul class="nav navbar-nav">
            <li> <a class="" href="#whoweare" style="font-weight: 600;">WHO WE ARE</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="" href="#whatwedo" style="font-weight: 600;">WHAT WE DO</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="" href="#patients" style="font-weight: 600;">WHAT WE OFFER</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="" href="#referapatients" style="font-weight: 600;">REFER A PATIENT</a> </li>
            <li class="margin-Top10">.</li>
            <li> <a class="" href="#partners" style="font-weight: 600;">PARTNER WITH US</a> </li>
         </ul>
         <ul class="nav navbar-nav navbar-right">
            <li class="contact-active position-relative">
               <a href="javascript:void(0);" style="font-weight: 600; padding-top: 18px; padding-bottom: 1.5rem;" class="contactbtn text-capitalize">
                  User Login / Signup
               </a>
            </li>
            <li class="margin-Top-sr"></li>
         </ul>
      </div>
   </div>
</nav>
<section class="navbar-info" id="homepage">
   
   <div class="fh5co-cover fh5co-cover-style-2 js-full-height" data-stellar-background-ratio="0.5" data-next="yes">
      <div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="5500" style="margin-bottom: -30px">
         <ol class="carousel-indicators" style="bottom: 3%">
            <li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active"></li>
            <li data-target="#bootstrap-touch-slider" data-slide-to="1"></li>
            <li data-target="#bootstrap-touch-slider" data-slide-to="2"></li>
         </ol>
         <div class="carousel-inner" role="listbox" onclick="document.getElementById('navbar-primary-collapse').hide()">
            <div class="item active">
               <div class="bs-slider-overlay"></div>
               <div class="container">
                  <div class="row carousel_slide3">
                     <div class="slide-text slide_style_center impact_class">
                        <h1 data-animation="animated flipInX"> Our Impact </h1>
                        <p data-animation="animated lightSpeedIn " class="white-color-font carousel_p "> Did you know that post-hospitalization expenses (aka recuperation expenses) are covered under Health Insurance? To know more <a href="javascript:void(0);" style="color: blue; text-decoration: none; font-weight: 400" class="contactbtn">get in touch</a> with us.<br>
                           <br>
                           Rural infant mortality in India is at 89%. Our new flagship program, Mission Bhagyavathi,
                           helps mother and child right through the pregnancy and thereafter 
                           to ensure that both lead a healthy life. 
                        </p>
                        <br>
                        <br>
                        <br>
                     </div>
                  </div>
               </div>
            </div>
            <div class="item">
               <div class="bs-slider-overlay"></div>
               <div class="container">
                  <div class="row">
                     <div class="slide-text slide_style_center our_purpose" id="slide2_div" style="margin-top: -15%;">
                        <div class="purpose_class">
                           <h1 data-animation="animated flipInX" style="color: white"> Our Purpose</h1>
                           <p data-animation="animated lightSpeedIn">Help patients <span class="slogan-style">GET WELL. SOONER.</span></p>
                        </div><br>
                        <div class="serve_class">
                           <h1 data-animation="animated flipInX" style="color: white"> Who We Serve</h1>
                           <p data-animation="animated lightSpeedIn">Patients getting discharged and requiring<br>
                              post-hospitalisation care
                           </p>
                        </div>
                        <p id="know_more" data-animation="animated zoomInRight" style="padding-top: 40px; color: #666; margin-right: -10px"><a href="#partners" id="know_more_2nd" class="page-scroll btn btn-xl sr-button" style="background-color: 
                           #2f3790; border: #2f3790;" >Know more</a></p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="item">
               <div class="bs-slider-overlay"></div>
               <div class="container">
                  <div class="row carousel_slide3">
                     <div class="slide-text slide_style_center">
                        <h1 data-animation="animated flipInX"> Partner With Us</h1>
                        <p data-animation="animated lightSpeedIn " class="white-color-font carousel_p">We are looking for partners who share a passion to make healthcare easy and affordable. We are now inviting care professionals in Bengaluru to join our wide network of service providers. We will launch in other cities soon.</p>
                        <p data-animation="animated zoomInRight" style="text-align: center; padding-top: 40px; color: #666; margin-right: -10px"><a href="#partners" class="page-scroll btn btn-xl sr-button" style="background-color: 
                           #2f3790; border: #2f3790; margin-top: -20px;" >Know more</a></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="slider-wrapper mobile-slider">
         <img id="slide" src="images/mobile-carousel-image.png">
         <div class="jq-slider-data">
            <div id="slider-content" class="jq-inner-wrapper clearfix">
                <h2 id="title" data-animation="animated lightSpeedIn" class="animated lightSpeedIn">  </h2>
                <p id="fitstContent" data-animation="animated lightSpeedIn"  class="animated lightSpeedIn"> </p>
                <h2 id="titleTwo" data-animation="animated lightSpeedIn" class="animated lightSpeedIn">  </h2>
                <p id="secondContent" data-animation="animated lightSpeedIn" class="animated lightSpeedIn"> </p>
                <a href="" id="btnPrint" data-animation="animated lightSpeedIn" class="page-scroll animated lightSpeedIn"> </a>
            </div>
         </div>
      </div>
      <div class="container" style="text-align: center; position: relative; padding-top: 80px" id="whoweare">
         <div class="row">
            <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp; font-weight: 500; font-size: 35px;">Who We Are</h3>
            <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s" style="visibility: visible;animation-duration: 1s;animation-delay: 0.8s;animation-name: fadeInUp; color: black; margin-bottom: -20px; font-size: 13px">Ribbn care management services is an end-to-end Healthcare Management Services Company, with primary focus on post-hospitalisation care.
               Our care plans are customized to the needs of the patient. Our services are designed to help patients adhere to the discharge advice as well as perform all their routine jobs and enable them to <span class="slogan-style">GET WELL. SOONER.</span>
            </p>
         </div>
      </div>
      <div class="fh5co-pricing-style-2" id="whatwedo_pricingdiv" style="padding-top: none!important">
         <div class="container" style=" margin: 0 auto" id="whatwedo">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center float-text" style="position: relative;">
               <h2 class="fh5co-heading wow fadeInUp black-color-font center-block text-center" data-wow-duration="1s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">&nbsp;</h2>
               <div class="row">
                  <h2 class="fh5co-heading wow fadeInUp black-color-font center-block text-center" data-wow-duration="1s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp; padding: 5px 0 0; margin-top: -40px">What We Do</h2>
                  <p class="wow fadeInUp  black-color-font whatwedo_p" data-wow-duration="1s" data-wow-delay=".8s" style="margin: 0 auto -15px">At Ribbn, we help patients <span class="slogan-style">GET WELL. SOONER.</span></p><br>
                  <p class="wow fadeInUp  black-color-font whatwedo_p" data-wow-duration="1s" data-wow-delay=".8s" style="margin: 0 auto;">
                     We provide a Professional, Reliable, and Affordable, post-hospitalisation care management services.
                     Our social and health care professionals will help you manage all your needs.
                     <span><a href="javascript:void(0);" style="color: blue" class="contactbtn">Drop us a query</a></span> and we will get in touch with you.
                     Alternatively you could call us on <b> </b><a href="tel:+91-9964321682" style="color: #2f3790">+91-9964321682</a> 
                  </p>
                  <div class="pricing" style="top: 15px">
                     <div class="col-sm-12 col-xs-12 col-md-4 col-lg-4 pricing-item wow fadeInUp"  data-wow-duration="1s" data-wow-delay="1.1s" style="visibility: visible; animation-duration: 1s; animation-delay: 1.1s; animation-name: fadeInUp;" >
                        <div class="thumbnail padding1520">
                           <div class="pricing-item pricing-item--featured wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s" style="visibility: visible; animation-duration: 1s; animation-delay: 1.1s; animation-name: fadeInUp;">
                              <div class="pricing-price"><img src="images/hospital_discharge.jpg" class="img-responsive" style="height: 197px;" width="100%" alt=""/></div>
                              <p class="pricing-sentence text-center">Hospital Discharge</p>
                              <h3 class="pricing-title black-color-font" style="text-align: justify; font-size: 13px; font-family: 'Open Sans', sans-serif; word-spacing: 0px; line-height: 2rem">Our fine team of healthcare experts are available to help you navigate through the cumbersome processes and 
                                 formalities during the hospital discharge and help you make an easy transition from the hospital to the comforts of your home. 
                              </h3>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12 col-xs-12 col-md-4 col-lg-4 pricing-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.4s" style="visibility: visible; animation-duration: 1s; animation-delay: 1.4s; animation-name: fadeInUp;">
                        <div class="thumbnail padding1520">
                           <div class="pricing-item pricing-item--featured wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.7s" style="visibility: visible; animation-duration: 1s; animation-delay: 1.4s; animation-name: fadeInUp;">
                              <div class="pricing-price"><img src="images/post_op_period.jpg" class="img-responsive" width="100%" style="height: 197px" height="251" alt=""/></div>
                              <p class="pricing-sentence text-center" style="font-size: 24px">Post Operative Period</p>
                              <h3 class="pricing-title black-color-font" style="text-align: justify; font-size: 13px; line-height: 2rem">Undergoing an operation can be taxing and the need for medical supervision does not cease 
                                 even when you are at home. We have for you the right mix of professionals for handholding you through your every-day
                                 medical needs at home. 
                              </h3>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12 col-xs-12 col-md-4 col-lg-4 pricing-item wow fadeInUp " data-wow-duration="1s" data-wow-delay="1.7s" style="visibility: visible; animation-duration: 1s; animation-delay: 1.7s; animation-name: fadeInUp;">
                        <div class="thumbnail padding1520">
                           <div class="pricing-item pricing-item--featured wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.7s" style="visibility: visible; animation-duration: 1s; animation-delay: 1.7s; animation-name: fadeInUp;">
                              <div class="pricing-price" style="background-image: url(images/getting_well.jpg); background-size: cover; background-repeat: no-repeat; height: 199px;margin-top: 4px;"></div>
                              <p class="pricing-sentence text-center">Getting Well </p>
                              <h3 class="pricing-title black-color-font" style="text-align: justify; font-size: 13px; font-family: 'icomoon'; word-spacing: 0.5px; line-height: 2rem">Our post-operative health care team is available for you 24*7 to help you <span class="slogan-style">GET WELL. SOONER.</span> 
                                 Even after you are back to your daily life, our team is just a phone call away for any medical assistance 
                                 that you might be needing. 
                              </h3>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="wrapperone padding-top-40" id="patients" style="background-attachment: fixed; line-height: 22px; padding-bottom: 10px;    margin-top: 40px;">
         <div class="firstone">
            <h2 style="color: white; padding-top: 10px">What We Offer</h2>
            <p class="offer_p">Your life post-discharge need not leave you stressed, anxious and apprehensive.
               <a href="javascript:void(0);" style="text-decoration: none; color: lightblue" class="contactbtn">Contact us</a> and we will take care of all your post-hospitalisation needs.
            </p>
         </div>
         <div class="container" style="width: 95%;">
            <div class="row">
               <div class="whatweoffer_card col-md-4 col-xs-12 text-center">
                  <a href="home-care.php">
                     <div style="width: 8em; height: 8em; background-color: white; border-radius: 50%; display: flex; justify-content: center; align-items: center;"><img class="img_class img-responsive" src="images/home.svg" width="70" height="70" alt="" style="">
                     </div>
                  </a>
                  <div class="second_cards second-car-design-fix" style="width: 50%; margin: 10px auto">
                     <!-- <a class="" href="home-care.php"> -->
                        <a href="home-care.php">
                           <div class="what_we_offer_head" style="width: 200px; padding-bottom: 10px">Home Care</div>
                        </a>
                        <a href="home-care.php" class="anchor-design-fix">   
                           <p style="text-align: left;">24X7 Attendant, Nurse, Dietician, Cook, Doctor, Physiotherapist, Counsellor and Advisor at your doorstep.</p>
                        </a>
                     <!-- </a> -->
                  </div>
               </div>
               <div class="whatweoffer_card col-md-4 col-xs-12 text-center" style="margin: 0 0">
                  <div style="width: 8em; height: 8em; background-color: white; border-radius: 50%; display: flex; justify-content: center; align-items: center;"><img class="img_class" src="images/insuranceclaims.svg" width="70" height="70" alt="" style="margin-left: -11px">
                  </div>
                  <div class="second_cards" style="width: 50%; margin: 10px auto">
                     <span class="what_we_offer_head" style="width: 200px; padding-bottom: 10px">Insurance Claims</span>
                     <p style="text-align: left;">End-to-end policy claims management. New policy opening.</p>
                  </div>
               </div>
               <div class="whatweoffer_card col-md-4 col-xs-12 text-center">
                  <a href="concierge.php">
                     <div style="width: 8em; height: 8em; background-color: white; border-radius: 50%; display: flex; justify-content: center; align-items: center;"><img class="img_class reverse-image" src="images/concierge_new.svg" width="90" height="90" alt="" style="">
                     </div>
                  </a>
                     <div class="second_cards second-car-design-fix" style="width: 50%; margin: 10px auto">
                        <a href="concierge.php">
                           <div class="what_we_offer_head" style="width: 200px; padding-bottom: 10px">Concierge Services</div>
                        </a>
                        <a href="concierge.php" class="anchor-design-fix">
                           <p style="text-align: left;">Transport to hospital and back home. Calendarising your care plan. Alerts and Notifications.</p>
                        </a>
                     </div>
               </div>
            </div>
            <div class="row">
               <div class="whatweoffer_card col-md-4 col-xs-12 text-center">
                  <a href="medical-equipments.php">
                     <div style="width: 8em; height: 8em; background-color: white; border-radius: 50%; display: flex; justify-content: center; align-items: center;"><img class="img_class" src="images/medicines.svg" width="70" height="70" alt="" style="">
                     </div>
                  </a>
                  <div class="second_cards second-car-design-fix" style="width: 50%; margin: 10px auto">
                     <!-- <a href="medical-equipments.php"> -->
                        <a href="medical-equipments.php">
                            <div class="what_we_offer_head" id="meds_equip" style="width: 200px; padding-bottom: 10px">Medicines And Medical Equipment</div>
                        </a>
                        <a href="medical-equipments.php" class="anchor-design-fix">
                           <p style="text-align: left;">Procure medicines at a discount, hire/ buy specialized medical equipments.</p>
                        </a>
                     <!-- </a> -->
                  </div>
               </div>
               <div class="whatweoffer_card col-md-4 col-xs-12 text-center" style="margin: 0 0">
                  <div style="width: 8em; height: 8em; background-color: white; border-radius: 50%; display: flex; justify-content: center; align-items: center;"><img class="img_class" src="images/financial.svg" width="70" height="70" alt="" style="">
                  </div>
                  <div class="second_cards" style="width: 50%; margin: 10px auto">
                     <span class="what_we_offer_head" style="width: 200px; padding-bottom: 10px">Financial Help</span>
                     <p style="text-align: left;">Bridge loans to meet costs for post-hospitalization care. <br/></p>
                  </div>
               </div>
               <div class="whatweoffer_card col-md-4 col-xs-12 text-center">
                  <div style="width: 8em; height: 8em; background-color: white; border-radius: 50%; display: flex; justify-content: center; align-items: center;"><img class="img_class" src="images/augmented.svg" width="70" height="70" alt="" style="">
                  </div>
                  <div class="second_cards" style="width: 50%; margin: 10px auto">
                     <span class="what_we_offer_head" style="width: 200px; padding-bottom: 10px">Augmented Care</span>
                     <p style="text-align: left;">Advisory services on alternative and/ or augmented care, viz., physiotherapy, medical exercise, etc.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="container padding-bottom-50" id="referapatients">
         <div class="text-center referapatientscards">
            <h2 class="fh5co-heading wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;" onclick="document.getElementById('partners_focus').focus()">Refer a Patient</h2>
            <p class="black-color-font" id="partners_focus" style="padding-bottom: 40px">Come join us in our mission to make post-hospitalisation care easy & affordable!
               Whether you are a care giver, a nurse, an attendant or a doctor - refer your patient and let us handle all their needs.<br>
            </p>
         </div>
         <div class="row" style="min-height: auto">
            <div class="col-md-4 text-center referapatientscards" style="position: relative; height: 425px">
               <div class="pricing-price text-center" style="width: 130px; height: 130px; background-color: white; border-radius: 50%; margin: 0 auto"><img class="img_class" src="images/home.svg" alt="" style="width: 75%; height: 75%; padding-top: 30px; margin: 0 auto">
               </div>
               <p class="pricing-sentence text-center" style="font-size: 2rem; color: black; padding-top: 20px">Caregiver</p>
               <h3 class="pricing-title referpatients_p">
                  It's not easy providing care to a patient; and there's no harm in seeking professional help. Just let us know and we will help you help your patient. Our end-to-end care management services are designed to help patients <span class="slogan-style">GET WELL. SOONER.</span>
                  <br><br>
                  <span class="text-center referpatients_span">
                     <h4><a href="#" class="black-color-font" style="text-decoration: underline;"  data-toggle="modal" data-target="#refererN">Refer a Patient</a></h4>
                  </span>
               </h3>
            </div>
            <div class="col-md-4 text-center hospitalspan referapatientscards" style="position: relative;">
               <div class="pricing-price text-center" style="width: 130px; height: 130px; background-color: white; border-radius: 50%; margin: 0 auto"><img class="img_class" src="images/HOSPITAL.svg" alt="" style="width: 75%; height: 75%; padding-top: 30px; margin: 0 auto">
               </div>
               <p class="pricing-sentence" style="font-size: 2rem; color: black; padding-top: 20px">Hospital</p>
               <h3 class="pricing-title referpatients_p">
                  Making healthcare affordable and easy! 90% of patients do not return for follow up care; we understand their challenges. With a wide network of care providing partners (hospitals and clinics) we offer patients a trusted, reliable and affordable service to help them <span class="slogan-style">GET WELL. SOONER.</span> And increase value to our partners. Find out how Ribbn can benefit you!
                  <br><br>
                  <span class="text-center referpatients_span" id="referpatients_span_hospital">
                     <h4><a href="#" class="black-color-font"  data-toggle="modal" data-target="#refererN"><u>Refer a Patient</u></a><a href="#partners" style="color: black;"> | <u><span> <a href="#" class="black-color-font" style="text-decoration: underline;"  data-toggle="modal" data-target="#registration_hospital">Registration</a> </span></u></a></h4>
                  </span>
               </h3>
            </div>
            <div class="col-md-4 text-center referapatientscards" style="position: relative; height: 425px">
               <div class="pricing-price text-center" style="width: 130px; height: 130px; background-color: white; border-radius: 50%; margin: 0 auto"><img class="img_class" src="images/insuranceclaims.svg" alt="" style="width: 75%; height: 75%; padding-top: 30px; margin: 0 auto; margin-left: -11px">
               </div>
               <p class="pricing-sentence" style="font-size: 2rem; color: black; padding-top: 20px">Insurance/ TPA</p>
               <h3 class="pricing-title referpatients_p">
                  Our services are designed to help patients <span class="slogan-style">GET WELL. SOONER.</span> As your customers they will benefit with our plans designed to lower costs. As your reference we will help build customer loyalty. <br><br>
                  <span class="text-center referpatients_span">
                     <!-- <h4><a href="#haveQuery" class="black-color-font" style="text-decoration: underline;">Have a Query? Contact Us</a></h4> -->
                     <h4><a href="#" class="black-color-font" style="text-decoration: underline;"  data-toggle="modal" data-target="#refererN">Refer a Patient</a></h4>
                  </span>
               </h3>
            </div>
         </div>
      </div>
      <div class="container-fluid fh5co-features-style-1 text-center padding-top-70" id="partners" style="background-image: linear-gradient(rgba(16, 17, 153, 0.7),rgba(237, 42, 84, 0.7)),url(&quot;images/website_background.jpg&quot;); data-stellar-background-ratio=0.5; background-position: center center">
         <div class="container">
            <h2 class="fh5co-heading wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp; padding: 0">Partner With Us</h2>
            <p class="wow fadeInUp " data-wow-duration="1s" data-wow-delay=".8s" style="visibility: visible;animation-duration: 1s;animation-delay: 0.8s;animation-name: fadeInUp;padding-bottom: 50px;" id="partners_p">We are looking for partners who share a passion to make healthcare easy and affordable. We are now inviting care professionals in Bengaluru to join our wide network of service providers. We will launch in other cities soon.</p>
            <div class="row text-center">
               <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInUp text-center partnerwithus_card">
                  <div data-toggle="modal" data-target="#ambulance-services" class="pricing-price partner_img" style="width: 130px; height: 130px; background-color: white; border-radius: 50%; margin: 20px auto;
                     "><img class="img_class reverse-image" src="images/AMBULANCE.svg" width="100" height="100" alt="" style="padding-top: 30px">
                  </div>
                  <p class="pricing-sentence text-center white-color-font" style="font-size: 24px"> <a href="#" class="page-scroll " data-toggle="modal" data-target="#ambulance-services" style="text-decoration: none!important;">Ambulance Services</a> </p>
               </div>
               <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInUp text-center partnerwithus_card">
                  <div data-toggle="modal" data-target="#homecare" class="pricing-price partner_img" style="width: 130px; height: 130px; background-color: white; border-radius: 50%; margin: 20px auto;
                     "><img class="img_class" src="images/home.svg" width="90" height="90" alt="" style="padding-top: 30px">
                  </div>
                  <p class="pricing-sentence text-center white-color-font" style="font-size: 24px"> <a href="#" class="page-scroll " data-toggle="modal" data-target="#homecare" style="text-decoration: none!important;">Homecare Service Provider</a> </p>
               </div>
               <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInUp text-center partnerwithus_card">
                  <div data-toggle="modal" data-target="#medical-equipment" class="pricing-price partner_img" style="width: 130px; height: 130px; background-color: white; border-radius: 50%; margin: 20px auto;
                     "><img class="img_class" src="images/medicines.svg" width="90" height="90" alt="" style="padding-top: 30px">
                  </div>
                  <p class="pricing-sentence text-center white-color-font" style="font-size: 24px"> <a href="#" class="page-scroll " data-toggle="modal" data-target="#medical-equipment" style="text-decoration: none!important;">Medical Equipment Supplier</a> </p>
               </div>
            </div>
         </div>
      </div>
      <div class="contact-pop-overlay">
         <div class="contact-pop-main" style="display: none;">
            <h3> 
               Registration
               <span class="pull-right"> 
                  <img src="images/close.svg">
               </span>
            </h3>
            <form>
               <div class="form-group">
                   <div class="reg-input-wrapper">
                      <img src="images/user.svg">  
                      <input class="form-control" placeholder="Name">
                   </div>
               </div>
               <div class="form-group">
                   <div class="reg-input-wrapper">
                      <img src="images/mobile.svg">
                      <input class="form-control" placeholder="Mobile Number">
                   </div>
               </div>
               <div class="form-group">
                   <div class="reg-input-wrapper">
                      <img src="images/otp_mobile.svg">
                      <input class="form-control" placeholder="OTP">
                   </div>
               </div>
               <div class="form-group">
                   <div class="reg-input-wrapper">
                      <img src="images/open_lock.svg">
                      <input class="form-control" placeholder="New Password">
                   </div>
               </div>
               <div class="form-group">
                   <div class="reg-input-wrapper">
                      <img src="images/lock.svg">
                      <input class="form-control" placeholder="Confirm Password">
                   </div>
               </div>
               <p> 
                  Already have a Login? 
                  <a> Please Sign in Here </a> 
               </p>
               <button class="reg-btn"> 
                  Next
               </button>
            </form>
         </div>

         <div class="contact-pop-main" style="display: none">
            <h3> 
               Login
               <span class="pull-right"> 
                  <img src="images/close.svg">
               </span>
            </h3>
            <form>
               <div class="form-group">
                   <div class="reg-input-wrapper">
                      <img src="images/mobile.svg">  
                      <input class="form-control" placeholder="Mobile No">
                   </div>
               </div>
               <div class="form-group">
                   <div class="reg-input-wrapper">
                      <img src="images/otp_mobile.svg">
                      <input class="form-control" placeholder="Password">
                   </div>
               </div>

               <p class="text-right"> 
                  Already have a Login? 
                  <a> Please Sign in Here </a> 
               </p>
               <button class="reg-btn"> 
                  Login
               </button>
            </form>
         </div>

         <div class="contact-pop-main">
            <h3 class="no-bottom-gap"> 
               Forgot Password
               <span class="pull-right"> 
                  <img src="images/close.svg">
               </span>
            </h3>
            <p class="text-right"> 
                  or
                  <a> Login to your account </a> 
               </p>
            <form>
               <div class="form-group">
                   <div class="reg-input-wrapper">
                      <img src="images/mobile.svg">  
                      <input class="form-control" placeholder="Mobile No">
                   </div>
               </div>
               <div class="form-group">
                   <div class="reg-input-wrapper">
                      <img src="images/otp_mobile.svg">
                      <input class="form-control" placeholder="OTP">
                      <span> Resend </span>
                   </div>
               </div>

               
               <button class="verify-btn"> 
                  Verify OTP
               </button>
            </form>
         </div>
      </div>
      <?php  include("includes/modals.php"); ?>
      <footer>
         <div class="footer" id="footer">
            <div class="container">
                  <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
                     <h3 class="haveQueryDiv"><span>&nbsp;</span>About Us</h3>
                     <ul class="w3-ul w3-hoverable">
                        <li style="font-size: 14px; color: #a0a0a0; font-weight: 600;">Our Purpose</li>
                        <li style="padding-bottom: 18px;">Ribbn care management services is an end-to-end Healthcare Management Services Company, with primary focus on post-hospitalisation care. <br>
                           Our Motto: <b>To Serve. With Love.</b>
                        </li>
                     </ul>
                  </div>
                  <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
                     <h3 class="haveQueryDiv"><span>&nbsp;</span>Address</h3>
                     <ul class="footerul">
                        No. 104, Ist Floor, 4th Cross, 13th Main, HSR Layout 5th Sector, Bengaluru 560034
                     </ul>
                  </div>
                  <div class="col-lg-4  col-md-4 col-sm-6 col-xs-12 ">
                    
                     <h3 class="haveQueryDiv"> <span id="haveQuery">&nbsp;</span> Contact Us / Have a Query </h3>
                     <form method="post" id="query_form" action="">
                        <ul>
                           <li>Please provide your details below and we will get back to you shortly.</li>
                        </ul>
                        <fieldset class="form-group">
                           <div class="input-append newsletter-box">
                              <input type="text" class="form-control backgroung-color" id="queryemail" name="queryemail" placeholder="Email ">
                           </div>
                        </fieldset>
                        <fieldset class="form-group">
                           <textarea id="querymessage" placeholder="Your Message" class="form-control backgroung-color" rows="5" name="querymessage" aria-invalid="false" ></textarea>
                        </fieldset>
                        <button class="btn bg-gray" name="submit" value="Submit" style="background-color: 
                           #2f3790; border: #2f3790" id="last_submit">Submit</button>
                     </form>
                  </div>
            </div>
            <a data-target="#fh5co-page" alt="Back To Top" href="#fh5co-page" class="backtotop_btn"><img style="width:50px; height: 50px" src="images/top_arrow.svg"></a>
         </div>
         <div class="container-fluid">
            <div class="row footer_class">
               <div class="col-md-4 col-sm-12 col-xs-12 text-center copyright_class">
                  <p class="copyright_p for-desk-top"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>

                  <p class="copyright_p for-mobile"><span style="font-size: 13px;text-align: justify;">©</span> Ribbn Care Management Services Pvt. Ltd.
                  <span class="rights_span">All Rights Reserved.</span></p>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="social_icons">
                     <a target="_blank" rel="noopener" href="https://www.facebook.com/Ribbnhealth/" class="icon_class"><i class="fa fa-facebook"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://www.linkedin.com/company/ribbn/" class="icon_class"><i class="fa fa-linkedin"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="https://twitter.com/RibbnHealth" class="icon_class"><i class="fa fa-twitter"></i></a>
                     <a target="_blank" rel="noopener" style="margin-left: 30px" href="blog-headings.php" class="icon_class">
                        <i class="fa fa-rss-square" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="col-md-4 col-sm-12 col-xs-12" id="termsdiv" style="order: -1">
                  <a href="privacy.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:20px">PRIVACY POLICY</a>
                  <a href="termsofservice.php" target="_blank" rel="noopener" style="color: #2f3790; font-size: 14px; margin-right:-1em" class="terms">TERMS OF SERVICE</a>
               </div>
               <!--
               <span style="order:5; font-size: 14px; color: lightgrey; margin-left:5px;float:right;" class="version_number">v 0.02</span>
               -->
            </div>
         </div>
      </footer>
</section>
</div>
<script>

       $('.modal-checkbox').click(function() {
        if ($(this).is(':checked')) {
           $(this).parents('form').find('.modal-submit-btn').removeAttr('disabled');
        } else {
           $(this).parents('form').find('.modal-submit-btn').attr('disabled', 'disabled');
        }
    });

       var i = 0;

var images = [

  {
    title: 'Our Impact',
    titleTwo: '',
    contentOne:' Did you know that post-hospitalization expenses (aka recuperation expenses) are covered under Health Insurance? To know more <a href="javascript:void(0);" style="color: blue; text-decoration: none; font-weight: 400" class="contactbtn">get in touch</a> with us.',
    contentTwo:'Rural infant mortality in India is at 89%. Our new flagship program, Mission Bhagyavathi,helps mother and child right through the pregnancy and thereafter to ensure that both lead a healthy life.',
    img: 'images/mobile-carousel-image.png',
    btn: '',
    anchoValue: '',
    classToAdd: ''
  },
  {
    title: ' Our Purpose',
    titleTwo: ' Who We Serve',
    contentOne:'<span class="small-width-content" style="margin-left: 4rem">Help patients <b>GET WELL. SOONER.</b></span>',
    contentTwo:'<span class="small-width-content" style="margin-left: 4rem">Patients getting discharged and requiring post-hospitalisation care</span>',
    img: 'images/mobile-carousel-image.png',
    btn: 'Know more',
    anchoValue: '#partners'
  },
  {
    title: 'Partner With Us',
    titleTwo: '',
    contentOne:'We are looking for partners who share a passion to make healthcare easy and affordable. We are now inviting care professionals in Bengaluru to join our wide network of service providers. We will launch in other cities soon.',
    contentTwo:'',
    img: 'images/mobile-carousel-image.png',
    btn: 'Know more',
    anchoValue: '#partners',
    classToAdd: ''
  }
];

var time = 5000;

function changeImage() {


 document.getElementById('slide').src= images[i].img;
 document.getElementById('title').innerHTML = images[i].title;
 document.getElementById('titleTwo').innerHTML = images[i].titleTwo;
 document.getElementById('fitstContent').innerHTML = images[i].contentOne;
 document.getElementById('secondContent').innerHTML = images[i].contentTwo;
 document.getElementById('btnPrint').href = images[i].anchoValue;

if(images[i].btn === '') {
  document.getElementById('btnPrint').classList.add('display-none');
} else {
  document.getElementById('btnPrint').classList.remove('display-none');
  document.getElementById('btnPrint').innerHTML = images[i].btn;
}

  if( i < images.length - 1) {
    i++;
  } else {
    i = 0;
  }

  setTimeout("changeImage()", time);
  
}

function addAnimatedClass() {

   document.getElementById('title').classList.add('lightSpeedIn');

 document.getElementById('titleTwo').classList.add('lightSpeedIn');

 document.getElementById('fitstContent').classList.add(' lightSpeedIn');
 
 document.getElementById('secondContent').classList.add('lightSpeedIn');

}

document.getElementById('btnPrint').classList.add('display-none');

window.onload = changeImage;


   



   </script>
<?php include("includes/footer.php"); ?>
